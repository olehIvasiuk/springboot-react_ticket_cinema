package com.ticket.cinemaapp.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "movie_posters")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoviePoster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "poster_id")
    private Long id;

    @Column(name = "movie_poster_name", columnDefinition = "text")
    private String posterName;

    @Column(name = "movie_original_file_name", columnDefinition = "text")
    private String originalFileName;

    @Column(name = "poster_size")
    private Long size;

    @Column(name = "content_type", columnDefinition = "text")
    private String contentType;

    @Lob
    @Column(columnDefinition = "LONGBLOB")
    private byte[] imageBytes;

    @OneToOne(mappedBy = "moviePoster", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Movie movie;
}
