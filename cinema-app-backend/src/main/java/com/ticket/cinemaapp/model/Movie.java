package com.ticket.cinemaapp.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "movie_data", schema = "ticket_cinema_db")
@Getter
@Setter
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "movie_name", columnDefinition = "text")
    private String movieName;

    @Column(name = "release_year", length = 1024)
    private String year;

    @ElementCollection(targetClass = MovieGenre.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "movie_genres")
    @Column(name = "genre")
    private Set<MovieGenre> genres;

    @Column(length = 1024)
    private String allowableAge;

    @Column(name = "country", columnDefinition = "text")
    private String country;

    @Column(length = 1024)
    private String duration;

    @Column(length = 1024)
    private String ratingIMDB;

    @Column(name = "rental_period_from", columnDefinition = "date")
    private LocalDate rentalPeriodFrom;

    @Column(name = "rental_period_to", columnDefinition = "date")
    private LocalDate rentalPeriodTo;

    @Column(name = "movie_language", length = 1024)
    private String language;

    @Column(name = "original_name", columnDefinition = "text")
    private String originalName;

    @Column(name = "director", columnDefinition = "text")
    private String director;

    @Column(name = "budget", columnDefinition = "text")
    private String budget;

    @Column(name = "starring", columnDefinition = "text")
    private String starring;

    @Column(name = "movie_description", columnDefinition = "text")
    private String movieDescription;

    @Column(name = "cover_path", columnDefinition = "text")
    private String coverPath;


    @Column(columnDefinition = "int default 0")
    private int negVotesCount;

    @Column(columnDefinition = "int default 0")
    private int posVotesCount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "poster_id", referencedColumnName = "poster_id")
    private MoviePoster moviePoster;

    public Movie() {}

    public Movie(String movieName, String year, Set<MovieGenre> genres, String allowableAge, String country,
                 String duration, String ratingIMDB, LocalDate rentalPeriodFrom, LocalDate rentalPeriodTo,
                 String language, String originalName, String director, String budget, String starring,
                 String movieDescription, String coverPath) {
        this.movieName = movieName;
        this.year = year;
        this.genres = genres;
        this.allowableAge = allowableAge;
        this.country = country;
        this.duration = duration;
        this.ratingIMDB = ratingIMDB;
        this.rentalPeriodFrom = rentalPeriodFrom;
        this.rentalPeriodTo = rentalPeriodTo;
        this.language = language;
        this.originalName = originalName;
        this.director = director;
        this.budget = budget;
        this.starring = starring;
        this.movieDescription = movieDescription;
        this.coverPath = coverPath;
        this.negVotesCount = 0;
        this.posVotesCount = 0;
    }

    public Movie(Long id, String movieName, String year, Set<MovieGenre> genres, String allowableAge, String country,
                 String duration, String ratingIMDB, LocalDate rentalPeriodFrom, LocalDate rentalPeriodTo,
                 String language, String originalName, String director, String budget, String starring,
                 String movieDescription, String coverPath, int negVotesCount, int posVotesCount) {
        this.id = id;
        this.movieName = movieName;
        this.year = year;
        this.genres = genres;
        this.allowableAge = allowableAge;
        this.country = country;
        this.duration = duration;
        this.ratingIMDB = ratingIMDB;
        this.rentalPeriodFrom = rentalPeriodFrom;
        this.rentalPeriodTo = rentalPeriodTo;
        this.language = language;
        this.originalName = originalName;
        this.director = director;
        this.budget = budget;
        this.starring = starring;
        this.movieDescription = movieDescription;
        this.coverPath = coverPath;
        this.negVotesCount = negVotesCount;
        this.posVotesCount = posVotesCount;
    }
}
