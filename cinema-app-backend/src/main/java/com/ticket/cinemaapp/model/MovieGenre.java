package com.ticket.cinemaapp.model;

import lombok.Getter;

@Getter
public enum MovieGenre {

    ANIME("Аніме"),
    BIOGRAPHICAL("Біографічний"),
    ACTION("Бойовик"),
    WESTERN("Вестерн"),
    WAR("Військові"),
    DETECTIVE("Детектив"),
    CHILDREN("Дитячі"),
    DOCUMENTARY("Документальний"),
    DRAMA("Драма"),
    HISTORICAL("Історичний"),
    COMEDY("Комедія"),
    SHORT_FILM("Короткометражний"),
    CRIME("Кримінал"),
    MELODRAMA("Мелодрама"),
    MYSTERY("Містика"),
    HORROR("Жахи"),
    THRILLER("Трилер"),
    SCIENCE_FICTION("Фантастика");

    private final String ukrainianName;

    MovieGenre(String ukrainianName) {
        this.ukrainianName = ukrainianName;
    }

    @Override
    public String toString() {
        return this.name() + " (" + ukrainianName + ")";
    }
}