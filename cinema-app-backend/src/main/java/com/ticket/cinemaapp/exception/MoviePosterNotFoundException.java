package com.ticket.cinemaapp.exception;

public class MoviePosterNotFoundException extends RuntimeException{


    public MoviePosterNotFoundException(String message) {
        super(message);
    }
}
