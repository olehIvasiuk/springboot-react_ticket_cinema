package com.ticket.cinemaapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class DuplicateMovieException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public DuplicateMovieException(String message) {
        super(message);
    }
}
