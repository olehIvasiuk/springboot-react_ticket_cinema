package com.ticket.cinemaapp.controller;

import com.ticket.cinemaapp.dto.MovieDTO;
import com.ticket.cinemaapp.request.MovieCreationRequest;
import com.ticket.cinemaapp.service.impl.MovieServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "http://localhost:3000")
public class SearchMoviesController {

    private final MovieServiceImpl movieService;

    public SearchMoviesController(MovieServiceImpl movieService) {
        this.movieService = movieService;
    }

    @PostMapping("/")
    public ResponseEntity<List<MovieDTO>> getMovies(@RequestParam(required = false) String query) {
        List<MovieDTO> movieSearchResult = new ArrayList<>();
        if (query != null && !query.isEmpty()) {
            movieSearchResult = movieService.searchMovies(query);
        }

        return ResponseEntity.ok(movieSearchResult);
    }

    @PostMapping("/control-panel/movies")
    public ResponseEntity<List<MovieDTO>> getSortedMovies(@RequestParam(name = "sort", required = false, defaultValue = "new_to_old") String sort) {
        List<MovieDTO> movieInfos = switch (sort) {
            case "old_to_new" -> movieService.getAllMoviesSortedByOldToNew();
            case "a_to_z" -> movieService.getAllMoviesSortedByAToZ();
            case "z_to_a" -> movieService.getAllMoviesSortedByZToA();
            default -> movieService.getAllMoviesSortedByNewToOld();
        };
        return ResponseEntity.ok(movieInfos);
    }

    @GetMapping("/movie-info/{id}")
    public ResponseEntity<Optional<MovieDTO>> movieInfoDetails(@PathVariable(value = "id") long id) {
        Optional<MovieDTO> movieInfo = movieService.getMovieById(id);
        return ResponseEntity.ok(movieInfo);
    }

    @PostMapping("/control-panel/create-movie")
    public String movieCreatePost(@RequestBody MovieCreationRequest movieCreationRequest) {

        movieService.createMovie(movieCreationRequest);
        return "redirect:/control-panel/movies";
    }
}