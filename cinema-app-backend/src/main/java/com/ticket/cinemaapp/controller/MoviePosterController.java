package com.ticket.cinemaapp.controller;

import com.ticket.cinemaapp.model.MoviePoster;
import com.ticket.cinemaapp.service.impl.MoviePosterServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/posters")
@AllArgsConstructor
public class MoviePosterController {

    private MoviePosterServiceImpl moviePosterService;

    @PostMapping
    public ResponseEntity<MoviePoster> uploadPoster(@RequestParam("file") MultipartFile file) throws IOException {
        MoviePoster poster = new MoviePoster();
        poster.setPosterName(file.getName());
        poster.setOriginalFileName(file.getOriginalFilename());
        poster.setSize(file.getSize());
        poster.setContentType(file.getContentType());
        poster.setImageBytes(file.getBytes());
        MoviePoster savedPoster = moviePosterService.savePoster(poster);
        return new ResponseEntity<>(savedPoster, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePoster(@PathVariable Long id) {
        moviePosterService.deletePosterById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MoviePoster> getPoster(@PathVariable Long id) {
        MoviePoster poster = moviePosterService.findPosterById(id);
        return poster != null ? new ResponseEntity<>(poster, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MoviePoster> updatePoster(@PathVariable Long id, @RequestParam("file") MultipartFile file) throws IOException {
        MoviePoster updatedPoster = new MoviePoster();
        updatedPoster.setPosterName(file.getName());
        updatedPoster.setOriginalFileName(file.getOriginalFilename());
        updatedPoster.setSize(file.getSize());
        updatedPoster.setContentType(file.getContentType());
        updatedPoster.setImageBytes(file.getBytes());

        MoviePoster savedPoster = moviePosterService.updatePoster(id, updatedPoster);
        return savedPoster != null ? new ResponseEntity<>(savedPoster, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}