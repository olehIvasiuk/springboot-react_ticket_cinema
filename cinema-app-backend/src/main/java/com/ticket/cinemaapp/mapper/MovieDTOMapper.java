package com.ticket.cinemaapp.mapper;

import java.util.function.Function;

import com.ticket.cinemaapp.dto.MovieDTO;
import com.ticket.cinemaapp.model.Movie;
import org.springframework.stereotype.Service;

@Service
public class MovieDTOMapper implements Function<Movie, MovieDTO> {
    @Override
    public MovieDTO apply(Movie movie) {
        return new MovieDTO(
                movie.getId(),
                movie.getMovieName(),
                movie.getYear(),
                movie.getGenres(),
                movie.getAllowableAge(),
                movie.getCountry(),
                movie.getDuration(),
                movie.getRatingIMDB(),
                movie.getRentalPeriodFrom(),
                movie.getRentalPeriodTo(),
                movie.getLanguage(),
                movie.getOriginalName(),
                movie.getDirector(),
                movie.getBudget(),
                movie.getStarring(),
                movie.getMovieDescription(),
                movie.getCoverPath(),
                movie.getNegVotesCount(),
                movie.getPosVotesCount()
        );
    }
}
