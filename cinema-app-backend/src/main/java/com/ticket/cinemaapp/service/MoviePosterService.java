package com.ticket.cinemaapp.service;

import com.ticket.cinemaapp.model.MoviePoster;

public interface MoviePosterService {

    MoviePoster savePoster(MoviePoster moviePoster);

    void deletePosterById(Long id);

    MoviePoster findPosterById(Long id);

    MoviePoster updatePoster(Long id, MoviePoster updatedPoster);

}