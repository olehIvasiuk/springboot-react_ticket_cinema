package com.ticket.cinemaapp.service;

import com.ticket.cinemaapp.dto.MovieDTO;
import com.ticket.cinemaapp.request.MovieCreationRequest;

import java.util.List;
import java.util.Optional;


public interface MovieService {

    List<MovieDTO> getAllMoviesSortedByOldToNew();

    List<MovieDTO> getAllMoviesSortedByNewToOld();

    List<MovieDTO> getAllMoviesSortedByAToZ();

    List<MovieDTO> getAllMoviesSortedByZToA();

    List<MovieDTO> searchMovies(String searchString);

    void createMovie(MovieCreationRequest movieCreationRequest);

    Optional<MovieDTO> getMovieById(Long id);

    void deleteMovie(Long id);
}