package com.ticket.cinemaapp.service.impl;

import com.ticket.cinemaapp.dto.MovieDTO;
import com.ticket.cinemaapp.exception.DuplicateMovieException;
import com.ticket.cinemaapp.exception.MovieNotFoundException;
import com.ticket.cinemaapp.mapper.MovieDTOMapper;
import com.ticket.cinemaapp.model.Movie;
import com.ticket.cinemaapp.repository.MovieRepository;
import com.ticket.cinemaapp.request.MovieCreationRequest;
import com.ticket.cinemaapp.service.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final MovieDTOMapper movieDTOMapper;

    @Override
    public void createMovie(MovieCreationRequest movieCreationRequest) {
        String movieName = movieCreationRequest.movieName();
        if (movieRepository.existsByMovieName(movieName)) {
            throw new DuplicateMovieException(String.format("A movie with the name '%s' already exists", movieName));
        }

        Movie newMovieEntity = new Movie(
            movieCreationRequest.movieName(),
                movieCreationRequest.year(),
                movieCreationRequest.genres(),
                movieCreationRequest.allowableAge(),
                movieCreationRequest.country(),
                movieCreationRequest.duration(),
                movieCreationRequest.ratingIMDB(),
                movieCreationRequest.rentalPeriodFrom(),
                movieCreationRequest.rentalPeriodTo(),
                movieCreationRequest.language(),
                movieCreationRequest.originalName(),
                movieCreationRequest.director(),
                movieCreationRequest.budget(),
                movieCreationRequest.starring(),
                movieCreationRequest.movieDescription(),
                movieCreationRequest.coverPath()
        );
        movieRepository.save(newMovieEntity);
    }

    @Override
    public Optional<MovieDTO> getMovieById(Long id) {
        return movieRepository.findById(id)
                .map(movie -> Optional.ofNullable(movieDTOMapper.apply(movie)))
                .orElseThrow(() -> new MovieNotFoundException("Movie does not exist by this id " + id));
    }

    @Override
    public void deleteMovie(Long id) {
        if (!movieRepository.existsById(id)) {
            throw new MovieNotFoundException("Movie does not exist by this id " + id);
        }
        movieRepository.deleteById(id);
    }

    @Override
    public List<MovieDTO> getAllMoviesSortedByOldToNew() {
        return  movieRepository.findAllByOrderByRentalPeriodFromAsc()
                .stream()
                .map(movieDTOMapper)
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieDTO> getAllMoviesSortedByNewToOld() {
        return movieRepository.findAllByOrderByRentalPeriodFromDesc()
                .stream()
                .map(movieDTOMapper)
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieDTO> getAllMoviesSortedByAToZ() {
        return movieRepository.findAllByOrderByMovieNameAsc()
                .stream()
                .map(movieDTOMapper)
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieDTO> getAllMoviesSortedByZToA() {
        return movieRepository.findAllByOrderByMovieNameDesc()
                .stream()
                .map(movieDTOMapper)
                .collect(Collectors.toList());
    }

    @Override
    public List<MovieDTO> searchMovies(String searchString) {
        return movieRepository.findAllByMovieNameContainingIgnoreCase(searchString)
                .stream()
                .map(movieDTOMapper)
                .collect(Collectors.toList());
    }
}
