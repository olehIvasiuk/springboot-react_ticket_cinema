package com.ticket.cinemaapp.service.impl;

import com.ticket.cinemaapp.exception.MoviePosterNotFoundException;
import com.ticket.cinemaapp.model.MoviePoster;
import com.ticket.cinemaapp.repository.MoviePosterRepository;
import com.ticket.cinemaapp.service.MoviePosterService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class MoviePosterServiceImpl implements MoviePosterService {

    private final MoviePosterRepository moviePosterRepository;

    @Override
    public MoviePoster savePoster(MoviePoster moviePoster) {
        return moviePosterRepository.save(moviePoster);
    }

    @Override
    public void deletePosterById(Long id) {
        moviePosterRepository.deleteById(id);
    }

    @Override
    public MoviePoster findPosterById(Long id) {
        return moviePosterRepository.findById(id)
                .orElseThrow(() -> new MoviePosterNotFoundException("Movie Poster does not exist by this id " + id));
    }

    @Override
    public MoviePoster updatePoster(Long id, MoviePoster updatedPoster) {
        Optional<MoviePoster> optionalMoviePoster = moviePosterRepository.findById(id);
        if (optionalMoviePoster.isPresent()) {
            MoviePoster existingPoster = optionalMoviePoster.get();
            existingPoster.setPosterName(updatedPoster.getPosterName());
            existingPoster.setOriginalFileName(updatedPoster.getOriginalFileName());
            existingPoster.setSize(updatedPoster.getSize());
            existingPoster.setContentType(updatedPoster.getContentType());
            existingPoster.setImageBytes(updatedPoster.getImageBytes());
            return moviePosterRepository.save(existingPoster);
        } else {
            throw new MoviePosterNotFoundException("Value is not present" + id);
        }
    }
}
