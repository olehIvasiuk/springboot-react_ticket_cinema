package com.ticket.cinemaapp.storage;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDownloadUtil {

    private Path foundFile;

    public Resource getFileUsResource(String filename) {
        Path uploadDirectory = Paths.get("moviePosters");

        try {
            Files.list(uploadDirectory).forEach(file -> {
                if (file.getFileName().toString().startsWith(filename)) {
                    foundFile = file;
                    return;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (foundFile != null) {
            try {
                return new UrlResource(foundFile.toUri());
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

}