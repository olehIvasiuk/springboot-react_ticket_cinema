package com.ticket.cinemaapp.dto;

import com.ticket.cinemaapp.model.MovieGenre;

import java.time.LocalDate;
import java.util.Set;

public record MovieDTO(
        Long id,
        String movieName,
        String year,
        Set<MovieGenre> genres,
        String allowableAge,
        String country,
        String duration,
        String ratingIMDB,
        LocalDate rentalPeriodFrom,
        LocalDate rentalPeriodTo,
        String language,
        String originalName,
        String director,
        String budget,
        String starring,
        String movieDescription,
        String coverPath,
        Integer negVotesCount,
        Integer posVotesCount
) {
}
