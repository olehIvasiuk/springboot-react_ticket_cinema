package com.ticket.cinemaapp.media;

import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageService {

    private static final String IMAGE_DIRECTORY = "/uploads/";

    public byte[] getThumbnail(String imageName, int width, int height) throws IOException {
        Path imagePath = Paths.get(IMAGE_DIRECTORY, imageName);

        if (!Files.exists(imagePath)) {
            throw new FileNotFoundException("Изображение не найдено: " + imageName);
        }

        BufferedImage originalImage = ImageIO.read(imagePath.toFile());
        BufferedImage scaledImage = Scalr.resize(originalImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(scaledImage, "jpg", outputStream);


        return outputStream.toByteArray();
    }

    public byte[] getFullImage(String imageName) throws IOException {
        Path imagePath = Paths.get(IMAGE_DIRECTORY, imageName);

        if (!Files.exists(imagePath)) {
            throw new FileNotFoundException("Изображение не найдено: " + imageName);
        }

        return Files.readAllBytes(imagePath);
    }
}