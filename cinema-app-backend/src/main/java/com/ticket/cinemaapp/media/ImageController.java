package com.ticket.cinemaapp.media;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping("/uploads/{imageName}")
    public ResponseEntity<byte[]> getImage(@PathVariable(value = "imageName") String imageName,
                                           @RequestParam(required = false) boolean thumbnail,
                                           @RequestParam(required = false, defaultValue = "100") int width,
                                           @RequestParam(required = false, defaultValue = "100") int height) {
        try {
            byte[] imageData;

            if (thumbnail) {
                imageData = imageService.getThumbnail(imageName, width, height);
            } else {
                imageData = imageService.getFullImage(imageName);
            }

            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imageData);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}