package com.ticket.cinemaapp.repository;

import com.ticket.cinemaapp.model.MoviePoster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoviePosterRepository extends JpaRepository<MoviePoster, Long> {

}