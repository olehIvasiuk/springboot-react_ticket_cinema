package com.ticket.cinemaapp.repository;

import com.ticket.cinemaapp.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findAllByOrderByRentalPeriodFromAsc();
    List<Movie> findAllByOrderByRentalPeriodFromDesc();
    List<Movie> findAllByOrderByMovieNameAsc();
    List<Movie> findAllByOrderByMovieNameDesc();
    List<Movie> findAllByMovieNameContainingIgnoreCase(String searchString);
    boolean existsByMovieName(String movieName);
}
