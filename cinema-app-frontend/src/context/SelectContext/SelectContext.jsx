import React, {createContext, useContext, useState, useRef, useCallback} from "react";

const SelectContext = createContext();

export const SelectProvider = ({children, options, isMultiple, name, setFieldValue}) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isClosing, setIsClosing] = useState(false);
    const [isFocused, setIsFocused] = useState(false);
    const [highlightedIndex, setHighlightedIndex] = useState(null);
    const [selectedValues, setSelectedValues] = useState([]);
    const selectRef = useRef(null);
    const dropdownRef = useRef(null);
    const itemRefs = useRef([]);

    const toggleSelect = () => setIsOpen(!isOpen);

    const closeSelect = () => {
        setIsClosing(true);
        setTimeout(() => {
            setIsOpen(false);
            setIsClosing(false);
        }, 200);
    };

    const handleSelectItem = (item) => {
        if (item === null || item === undefined) return
        let updatedValues;

        if (isMultiple) {
            if (selectedValues.includes(item)) {
                updatedValues = selectedValues.filter(val => val !== item);
            } else {
                updatedValues = [...selectedValues, item];
            }
        } else {
            updatedValues = [item];
            closeSelect();
        }

        setSelectedValues(updatedValues);
        setFieldValue(name, updatedValues);
    };

    const handleNavigate = useCallback((direction) => {
        let newIndex;

        if (direction === "up") {
            newIndex = highlightedIndex > 0 ? highlightedIndex - 1 : options.length - 1;
        } else if (direction === "down") {
            newIndex = highlightedIndex === null || highlightedIndex === options.length - 1 ? 0 : highlightedIndex + 1;
        }

        setHighlightedIndex(newIndex);

        if (itemRefs.current[newIndex]) {
            itemRefs.current[newIndex].scrollIntoView({
                behavior: "smooth",
                block: "nearest",
            });
        }
    }, [highlightedIndex, options]);

    return (
        <SelectContext.Provider
            value={{
                isOpen,
                isClosing,
                highlightedIndex,
                selectedValues,
                itemRefs,
                options,
                isMultiple,
                toggleSelect,
                closeSelect,
                setHighlightedIndex,
                handleSelectItem,
                handleNavigate,
                isFocused,
                setIsFocused,
                selectRef,
                dropdownRef,
            }}
        >
            {children}
        </SelectContext.Provider>
    );
};

export const useSelectContext = () => useContext(SelectContext);