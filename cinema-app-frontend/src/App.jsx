import './assets/styles/cite-global.css';
import Header from './components/site/header/Header';
import SectionMain from './components/site/main/MainContent.jsx';
import Footer from './components/site/footer/Footer';

function App() {
    return (
        <div className='tt_root-wrapper'>
            <Header/>
            <SectionMain/>
            <Footer/>
        </div>
    );
}


export default App;