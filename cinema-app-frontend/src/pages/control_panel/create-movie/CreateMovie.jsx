import React, {useState, useEffect, useMemo} from 'react';
import {Form} from "react-router-dom";
import TextField from "../../../components/control_panel/fields/TextField.jsx";
import NumberField from "../../../components/control_panel/fields/NumberField.jsx";
import DateFromTo from "../../../components/control_panel/fields/DateFromTo.jsx";
import TextAreaField from "../../../components/control_panel/fields/TextAreaField.jsx";
import "./CreateMovie.css";
import MultiSelect from "../../../components/control_panel/fields/MultiSelect.jsx";

const CreateMovie = () => {
    const genres = useMemo(() => ['Аніме', 'Біографічний', 'Бойовик', 'Вестерн', 'Військові', 'Детектив', 'Дитячі',
        'Документальний', 'Драма', 'Історичний', 'Комедія', 'Короткометражний', 'Кримінал',
        'Мелодрама', 'Містика', 'Жахи', 'Трилер', 'Фантастика'], []);

    const countries = useMemo(() => ['США (United States)', 'Україна (Ukraine)', 'Великобританія (United Kingdom)',
        'Франція (France)', 'Індія (India)', 'Китай (China)', 'Японія (Japan)', 'Корея (South Korea)',
        'Німеччина (Germany)', 'Іспанія (Spain)', 'Італія (Italy)', 'Канада (Canada)', 'Австралія (Australia)',
        'Бразилія (Brazil)', 'Мексика (Mexico)', 'Швеція (Sweden)', 'Данія (Denmark)', 'Норвегія (Norway)',
        'Південна Африка (South Africa)', 'Нова Зеландія (New Zealand)'], []);

    const options = useMemo(() => ['0+', '6+', '12+', '16+', '18+'], []);

    const languages = useMemo(() => ['Українська, Дубльований', 'Оригінальна звукова доріжка',
        'Двомовна (українська та оригінальна звукові доріжки одночасно)', 'Німецька, дубльований', 'Італійська, дубльований',
        'Іспанська, дубльований', 'Японська, дубльований', 'Китайська, дубльований', 'Польська, дубльований',
        'Субтитри українською'], []);

    const [formData, setFormData] = useState({
        movieName: '',
        year: '',
        genre: [],
        allowableAge: '',
        country: [],
        language: [],
        duration: '',
        ratingIMDB: '',
        rentalPeriodFrom: '',
        rentalPeriodTo: '',
        originalName: '',
        director: '',
        budget: '',
        starring: '',
        movieDescription: ''
    });

    const [errors, setErrors] = useState({});

    const validateField = (name, value) => {
        const validations = {
            movieName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/.test(value) && value.trim().length > 0,
            year: /^\d{4}$/.test(value) && value.trim().length > 0 && (value >= 1900 && value <= 3000),
            duration: /^\d{1,3}$/.test(value) && value <= 500,
            ratingIMDB: /^(\d{1,2}(\.\d{1,2})?)$/.test(value) && value <= 10,
            rentalPeriodFrom: value.trim().length > 0,
            rentalPeriodTo: value.trim().length > 0,
            originalName: /^(?!.*[\\?!.,:-]{2})[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ\s\\?!.,:-]+$/.test(value) && value.trim().length > 0,
            director: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ\s]+$/u.test(value) && value.trim().length > 0,
            budget: /^[а-яА-Яa-zA-Z0-9ґҐєЄіІїЇ$€£¥₴\s]+$/u.test(value) && value.trim().length > 0,
            starring: /^[а-яА-Яa-zA-ZґҐєЄіІїЇ,.«»"\s]+$/u.test(value) && value.trim().length > 0,
            movieDescription: value.length !== 3000 && value.trim().length > 0
        };
        return validations[name];
    };

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setFormData({
            ...formData,
            [name]: value
        });

        setErrors({
            ...errors,
            [name]: !validateField(name, value)
        });
    };

    const handleMultiSelectChange = (name, values) => {
        setFormData({
            ...formData,
            [name]: values
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const newErrors = {};

        Object.keys(formData).forEach(name => {
            if (!validateField(name, formData[name])) {
                newErrors[name] = true;
            }
        });

        if (Object.keys(newErrors).length === 0) {
            console.log('Submitting form data:', formData);
            fetch('/control-panel/create-movie', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            })
                .then(response => response.json())
                .then(data => console.log('Success:', data))
                .catch(error => console.error('Error:', error));
        } else {
            setErrors(newErrors);
        }
    };

    useEffect(() => {
        console.log(errors);
    }, [errors]);

    return (
        <div className="tt_create-movie__form-container">
            <Form action={"/control-panel/create-movie"} method="post" name="create-movie__form" id="form"
                  encType="multipart/form-data" onSubmit={handleSubmit} noValidate>
                <div className="tt_create-movie__form-body">
                    <section className="tt_create-movie__section head">
                        <div className="tt_create-movie__upload-post"></div>
                        <TextField
                            name="movieName"
                            title="Назва фільму"
                            id="movie-name__inp"
                            tip={`Допустимі символи: букви, цифри, пробіли та . , ? ! - . ' "`}
                            value={formData.movieName}
                            onChange={handleInputChange}
                            error={errors.movieName}
                        />
                    </section>

                    <section className="tt_create-movie__section details">
                        <NumberField
                            name="year"
                            title="Рік релізу"
                            id="year__inp"
                            maxLength="4"
                            tip={`Число з 4-ох цифр. Наприклад: "2024". Мінімально від 1900р.`}
                            value={formData.year}
                            onChange={handleInputChange}
                            error={errors.year}
                        />

                        <MultiSelect
                            name="genre"
                            title="Жанр"
                            id="genre__inp"
                            isMultiple={true}
                            options={genres}
                            onChange={handleMultiSelectChange}
                            required={true}
                        />

                        <MultiSelect
                            name="allowableAge"
                            title="Допустимий вік"
                            isMultiple={false}
                            id="allowable-age__inp"
                            options={options}
                            onChange={handleMultiSelectChange}
                            required={true}
                        />

                        <MultiSelect
                            name="country"
                            title="Країна"
                            isMultiple={true}
                            id="country__inp"
                            options={countries}
                            onChange={handleMultiSelectChange}
                            required={true}
                        />

                        <NumberField
                            name={"duration"}
                            title={"Тривалість"}
                            id={"duration__inp"}
                            maxLength={"3"}
                            tip={`Вкажіть тривалість фільму в хвилинах, від 0 до 500`}
                            value={formData.duration}
                            onChange={handleInputChange}
                            error={errors.duration}
                        />

                        <NumberField
                            name={"ratingIMDB"}
                            title={"Рейтинг IMDB"}
                            id={"ratingIMDB__inp"}
                            tip={`Рейтинг має бути в числовому діапазоні: від "0.00" до "10.00"`}
                            value={formData.ratingIMDB}
                            onChange={handleInputChange}
                            error={errors.ratingIMDB}
                        />

                        <DateFromTo
                            titleFrom={"Період прокату. Від:"}
                            titleTo={"Період прокату. До:"}
                            nameFrom={"rentalPeriodFrom"}
                            nameTo={"rentalPeriodTo"}
                            idFrom={"rental-period-from__inp"}
                            idTo={"rental-period-to__inp"}
                            fromValue={formData.rentalPeriodFrom}
                            toValue={formData.rentalPeriodTo}
                            onFromChange={handleInputChange}
                            onToChange={handleInputChange}
                            errorFrom={errors.rentalPeriodFrom}
                            errorTo={errors.rentalPeriodTo}
                        />

                        <MultiSelect
                            name="language"
                            title="Мова"
                            id="language__inp"
                            isMultiple={true}
                            options={languages}
                            onChange={handleMultiSelectChange}
                            required={true}/>

                        <TextField
                            title={"Оригінальна назва"}
                            name={"originalName"}
                            id={"original-name__inp"}
                            tip={`Допустимі символи: букви, цифри, пробіли та . , ? ! - . "`}
                            value={formData.originalName}
                            onChange={handleInputChange}
                            error={errors.originalName}
                        />

                        <TextField
                            title={"Режисер"}
                            name={"director"}
                            id={"director__inp"}
                            tip={`Допустимі символи: букви та пробіли`}
                            value={formData.director}
                            onChange={handleInputChange}
                            error={errors.director}
                        />

                        <TextField
                            title={"Бюджет"}
                            name={"budget"}
                            id={"budget__inp"}
                            tip={`Допустимі символи: букви, цифри, пробіли та $ € £ ¥ ₴`}
                            value={formData.budget}
                            onChange={handleInputChange}
                            error={errors.budget}
                        />

                        <TextField
                            title={"У головних ролях"}
                            name={"starring"}
                            id={"starring__inp"}
                            tip={`Допустимі символи: букви, пробіли та . , « » "`}
                            value={formData.starring}
                            onChange={handleInputChange}
                            error={errors.starring}
                        />
                    </section>

                    <section className="tt_create-movie__section desc">
                        <TextAreaField
                            title="Опис фільму"
                            name="movieDescription"
                            id="movie-description__inp"
                            tip={` символів введено. Максимально: 3000`}
                            maxLength={3000}
                            value={formData.movieDescription}
                            onChange={handleInputChange}
                            error={errors.movieDescription}
                        />
                    </section>

                    <div className="tt_create-movie__form-controls">
                        <button className="act tt_create-movie__create-action" role="button" type="submit">Створити
                        </button>
                    </div>
                </div>
            </Form>
        </div>
    );
};

export default CreateMovie;
