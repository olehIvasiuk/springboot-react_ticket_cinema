import React, {memo, useState} from 'react';
import Header from "../../../components/control_panel/header/Header.jsx";
import "./General.css"
import AsideMenu from "../../../components/control_panel/aside-menu/AsideMenu.jsx";
import MainContent from "../../../components/control_panel/cp-main/MainContent.jsx";


const General = memo(() => {
    const [isExpanded, setExpanded] = useState(false);

    return (
        <div className="tt_wrapper">
            <Header setExpanded={setExpanded}/>
            <div className="tt_base-container">
                <AsideMenu isExpanded={isExpanded}/>
                <MainContent/>
            </div>
        </div>
    );
});

export default General;