import React from 'react';
import cn from "classnames";
import PropTypes from 'prop-types';
import st from "@ui/Modal/Modal.module.css";

const ModalBody = ({children, className}) => {

    const bodyClasses = cn(
        st.ModalBody,
        className,
    );

    return (
        <div className={bodyClasses}>
            {children}
        </div>
    )
};

ModalBody.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};

export default ModalBody;