import React from 'react';
import cn from "classnames";
import PropTypes from 'prop-types';
import st from "@ui/Modal/Modal.module.css";

const ModalFooter = ({children, className}) => {

    const footerClasses = cn(
        st.ModalFooter,
        className,
    );

    return (
        <div className={footerClasses}>
            {children}
        </div>
    )
};

ModalFooter.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};

export default ModalFooter;