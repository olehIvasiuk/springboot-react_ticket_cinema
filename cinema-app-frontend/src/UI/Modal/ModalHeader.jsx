import React from 'react';
import PropTypes from 'prop-types';
import cn from "classnames";
import st from "@ui/Modal/Modal.module.css";
import Button from "@ui/Button/Button.jsx";
import {useModalContext} from "@ui/Modal/Modal.jsx";

const ModalHeader = ({children, className}) => {
    const {hideCloseButton, onClose} = useModalContext();

    const headerClasses = cn(
        st.ModalHeader,
        className,
    );

    return (
        <div className={headerClasses}>
            {children}
            {!hideCloseButton &&
                <Button
                    className={st.ModalClose}
                    isIconOnly={true}
                    iconId="icon-red-cross"
                    onClick={onClose}
                />
            }
        </div>
    );
};

ModalHeader.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
};

export default ModalHeader;
