import React from 'react';
import PropTypes from 'prop-types';

const ModalContent = ({children}) => (
    <>
        {children}
    </>
);

ModalContent.propTypes = {
    children: PropTypes.node,
};

export default ModalContent;