import React, {createContext, useContext, useEffect, useState, useRef, useMemo} from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import FocusTrap from "focus-trap-react";
import st from "@ui/Modal/Modal.module.css";
import Portal from "@utils/Portal/Portal.jsx";
import {useDisableScroll} from "@hooks/UseDisableScroll/useDisableScroll.js";

const ModalContext = createContext();
export const useModalContext = () => useContext(ModalContext);

const Modal = ({
                   children,
                   className,
                   size = "md",
                   rounded = "md",
                   backdrop = "blur",
                   animationVariant = "entry",
                   isDismissible = true,
                   isEscDismissible = true,
                   hideCloseButton = false,
                   isOpen,
                   onClose,
                   ariaLabelledBy,
               }) => {
    const [isClosing, setIsClosing] = useState(false);
    const [mouseDownTarget, setMouseDownTarget] = useState(null);
    const modalRef = useRef(null);
    useDisableScroll(isOpen);

    const handleClose = () => {
        setIsClosing(true);
        setTimeout(() => {
            onClose();
            setIsClosing(false);
        }, 300);
    };

    const keydownHandler = ({key}) => {
        if (key === "Escape" && isEscDismissible) {
            handleClose();
        }
    };

    useEffect(() => {
        if (isOpen) {
            document.addEventListener("keydown", keydownHandler);
        }
        return () => document.removeEventListener("keydown", keydownHandler);
    }, [isOpen, isEscDismissible]);

    useEffect(() => {
        const handleMouseDown = (event) => {
            if (!modalRef.current.contains(event.target)) {
                setMouseDownTarget(event.target);
            }
        };

        const handleMouseUp = (event) => {
            if (mouseDownTarget && !modalRef.current.contains(event.target) && isDismissible) {
                handleClose();
            }
            setMouseDownTarget(null);
        };

        if (isOpen) {
            document.addEventListener("mousedown", handleMouseDown);
            document.addEventListener("mouseup", handleMouseUp);
        }

        return () => {
            document.removeEventListener("mousedown", handleMouseDown);
            document.removeEventListener("mouseup", handleMouseUp);
        };
    }, [isOpen, mouseDownTarget, isDismissible]);

    const modalClasses = cn(
        st.ModalContent,
        {
            [st.SlideUp]: !isClosing ? animationVariant === "slide" : undefined,
            [st.EntryIn]: !isClosing ? animationVariant === "entry" : undefined,
            [st.LightSpeedIn]: !isClosing ? animationVariant === "light-speed" : undefined,
        }, {
            [st.SlideDown]: isClosing ? animationVariant === "slide" : undefined,
            [st.EntryOut]: isClosing ? animationVariant === "entry" : undefined,
            [st.LightSpeedOut]: isClosing ? animationVariant === "light-speed" : undefined,
        }, {
            [st.SizeXs]: size === "xs",
            [st.SizeSm]: size === "sm",
            [st.SizeMd]: size === "md",
            [st.SizeLg]: size === "lg",
            [st.SizeXl]: size === "xl",
            [st.SizeFull]: size === "full",
            [st.SizeAuto]: size === "auto",
        }, {
            [st.RoundedSm]: rounded === "sm",
            [st.RoundedMd]: rounded === "md",
            [st.RoundedLg]: rounded === "lg",
            [st.RoundedXl]: rounded === "xl",
            [st.RoundedNone]: rounded === "none",
        },
        className
    );

    const backdropClasses = cn(
        st.ModalBackdrop,
        {[st.Hide]: isClosing},
        {
            [st.Darken]: backdrop === "darken",
            [st.Blur]: backdrop === "blur",
        }
    );

    const contextValue = useMemo(() => ({
            onClose: handleClose, hideCloseButton
        }
    ), [handleClose, hideCloseButton]);

    return isOpen ? (
        <Portal containerElement={document.body}>
            <FocusTrap active={isOpen} focusTrapOptions={{
                escapeDeactivates: false
            }}>
                <div
                    className={st.Modal}
                    aria-labelledby={ariaLabelledBy}
                    role="dialog"
                    aria-modal="true"
                >
                    <div className={backdropClasses}></div>
                    <div className={modalClasses} ref={modalRef}>
                        <ModalContext.Provider value={contextValue}>
                            {typeof children === "function" ? children({onClose: handleClose}) : children}
                        </ModalContext.Provider>
                    </div>
                </div>
            </FocusTrap>
        </Portal>
    ) : null;
};

Modal.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
    className: PropTypes.string,
    size: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl", "full", "auto"]),
    rounded: PropTypes.oneOf(["sm", "md", "lg", "xl", "none"]),
    backdrop: PropTypes.oneOf(["darken", "blur"]),
    animationVariant: PropTypes.oneOf(["slide", "entry", "light-speed"]),
    isDismissible: PropTypes.bool,
    isEscDismissible: PropTypes.bool,
    hideCloseButton: PropTypes.bool,
    isOpen: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    ariaLabelledBy: PropTypes.string,
};

export default Modal;