import React, { memo } from "react";
import cn from "classnames";
import PropTypes from "prop-types";
import Icon from "@ui/Icon/Icon.jsx";
import st from "@ui/Button/Button.module.css";

const Button = ({
                    className,
                    size = "md",
                    rounded = "md",
                    color = "default",
                    variant = "solid",
                    isIconOnly = false,
                    iconId,
                    iconX = 24,
                    iconY = 24,
                    iconColor,
                    isLoading = false,
                    isDisabled = false,
                    type = "button",
                    role = "button",
                    isHidden = false,
                    tabIndex = 0,
                    ariaLabel,
                    children,
                    ...props
                }) => {
    const buttonClass = cn(
        st.Button,
        {
            [st.Size_sm]: size === "sm",
            [st.Size_md]: size === "md",
            [st.Size_lg]: size === "lg",
        },
        {
            [st.Rounded_sm]: rounded === "sm",
            [st.Rounded_md]: rounded === "md",
            [st.Rounded_lg]: rounded === "lg",
            [st.Rounded_full]: rounded === "full",
            [st.Rounded_none]: rounded === "none",
        },
        {
            [st.Default]: color === "default",
            [st.Primary]: color === "primary",
            [st.Secondary]: color === "secondary",
        },
        {
            [st.Variant_solid]: variant === "solid",
            [st.Variant_outline]: variant === "outline",
            [st.Variant_ghost]: variant === "ghost",
        },
        {
            [st.IconOnly]: isIconOnly,
        },
        className
    );

    const textClasses = cn(st.Text_content, {
        [st.Text_content_hidden]: isLoading,
    });

    return (
        <button
            className={buttonClass}
            disabled={isDisabled || isLoading}
            type={type}
            role={role}
            hidden={isHidden}
            tabIndex={tabIndex}
            aria-busy={isLoading}
            aria-disabled={isDisabled || isLoading}
            aria-label={ariaLabel}
            {...props}
        >
            {isLoading ? (
                <span className={st.Spinner} aria-hidden="true"></span>
            ) : (
                iconId && (
                    <Icon iconId={iconId} iconX={iconX} iconY={iconY} color={iconColor}/>
                )
            )}
            {children && <span className={textClasses}>{children}</span>}
        </button>
    );
};

Button.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    size: PropTypes.oneOf(["sm", "md", "lg"]),
    rounded: PropTypes.oneOf(["sm", "md", "lg", "full", "none"]),
    color: PropTypes.oneOf(["default", "primary", "secondary"]),
    variant: PropTypes.oneOf(["solid", "outline", "ghost"]),
    isIconOnly: PropTypes.bool,
    iconId: PropTypes.string,
    iconX: PropTypes.number,
    iconY: PropTypes.number,
    iconColor: PropTypes.string,
    isLoading: PropTypes.bool,
    isDisabled: PropTypes.bool,
    type: PropTypes.oneOf(["button", "submit", "reset"]),
    role: PropTypes.string,
    isHidden: PropTypes.bool,
    tabIndex: PropTypes.number,
    ariaLabel: PropTypes.string,
};

export default memo(Button);