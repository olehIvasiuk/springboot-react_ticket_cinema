import React from 'react';
import sprite from "@assets/icons/svg/cite-sprites.svg"

const Icon = ({
                  color = "E4E4E7",
                  iconId,
                  iconY = 24,
                  iconX = 24
              }) => {
    return (
        <svg width={iconX} height={iconY} color={color}>
            <use xlinkHref={`${sprite}#${iconId}`}></use>
        </svg>
    );
};

export default Icon;
