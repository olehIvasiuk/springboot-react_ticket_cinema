import React from 'react';
import cn from "classnames";
import st from "@ui/Separator/Separator.module.css"

const Separator = ({text, color, bg = '#d4d4d8', mtb, mt, mb, className = "", height = "1", size = 14}) => {
    const separatorStyle = {
        backgroundColor: `var(${bg})`,
        height: `${height}px`,
    };

    const separatorTextStyle = {
        color: `var(${color})`,
        fontSize: `${size}px`,
    };

    const containerStyle = {
        ...(mtb && {margin: `${mtb}rem 0`}),
        ...(mt && {marginTop: `${mt}rem`}),
        ...(mb && {marginBottom: `${mb}rem`}),
    };

    return (
        <div
            className={cn(st.Separator, className)}
            style={containerStyle}
        >
            <hr className={st.Line} style={separatorStyle}/>
            {text && <span className={st.Title} style={separatorTextStyle}>{text}</span>}
            {text && <hr className={st.Line} style={separatorStyle}/>}
        </div>
    );
};

export default Separator;