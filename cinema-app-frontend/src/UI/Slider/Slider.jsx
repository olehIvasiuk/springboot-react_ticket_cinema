import React, {memo, useEffect, useRef, useState, useCallback, useMemo} from "react";
import cn from "classnames";
import st from "@ui/Slider/Slider.module.css"
import PropTypes from "prop-types";

/**
 * @typedef {Object} SliderProps
 * @property {number} [min=0] - The minimum value of the slider.
 * @property {number} [max=100] - The maximum value of the slider.
 * @property {number} [step=1] - The step value for the slider.
 * @property {function} [onChange] - Callback function when the slider value changes.
 * @property {number} [initialValue=0] - The initial value of the slider.
 * @property {boolean} [disabled=false] - Whether the slider is disabled.
 * @property {string} [accent="sea"] - The style of the slider.
 * @property {string} [orientation="horizontal"|"vertical"] - The orientation of the slider.
 * @property {boolean} [marks=false] - Whether to show marks on the track.
 */

/**
 * Slider component.
 * @param {SliderProps} props
 */
const Slider = memo(({
                         min = 0,
                         max = 100,
                         step = 1,
                         onChange,
                         value = 0,
                         disabled = false,
                         accent = "sea",
                         orientation = "horizontal",
                         marks = false
                     }) => {
    const sliderRef = useRef(null);
    const thumbRef = useRef(null);

    const [isShowLabel, setShowLabel] = useState(false);
    const [isDragging, setDragging] = useState(false);

    const calculateNewValue = useCallback((clientPos) => {
        if (!sliderRef.current) return value;
        const rect = sliderRef.current.getBoundingClientRect();
        const newPercentage = orientation === "horizontal"
            ? (clientPos - rect.left) / rect.width
            : (rect.bottom - clientPos) / rect.height;
        const newValue = min + Math.round(newPercentage * (max - min) / step) * step;
        return Math.max(min, Math.min(newValue, max));
    }, [min, max, step, value, orientation]);

    const handleMove = useCallback((e) => {
        const clientPos = e.touches ? (orientation === "horizontal" ? e.touches[0].clientX : e.touches[0].clientY)
            : (orientation === "horizontal" ? e.clientX : e.clientY);
        const newValue = calculateNewValue(clientPos);
        if (onChange) onChange(Number(newValue.toFixed(1)));
    }, [calculateNewValue, orientation, onChange]);

    const handleStart = useCallback((e) => {
        e.preventDefault();
        setShowLabel(true);
        setDragging(true);
        document.addEventListener(e.type === "mousedown" ? "mousemove" : "touchmove", handleMove, {passive: true});
        document.addEventListener(e.type === "mousedown" ? "mouseup" : "touchend", handleEnd);
    }, [handleMove]);

    const handleEnd = useCallback((e) => {
        setShowLabel(false);
        setDragging(false);
        document.removeEventListener("mousemove", handleMove);
        document.removeEventListener("mouseup", handleEnd);
        document.removeEventListener("touchmove", handleMove);
        document.removeEventListener("touchend", handleEnd);
    }, [handleMove]);

    const position = useMemo(() => ((value - min) / (max - min)) * 100, [value, min, max]);

    const marksArray = useMemo(() => {
        if (!marks) return [];
        const marksList = [];
        for (let i = min; i <= max; i += step) {
            const percentage = ((i - min) / (max - min)) * 100;
            marksList.push({
                value: i,
                position: percentage
            });
        }
        return marksList;
    }, [min, max, step, marks]);

    const handleKeyDown = useCallback((e) => {
        if (disabled) return;
        let newValue = value;
        if (e.key === "ArrowRight" || e.key === "ArrowUp") {
            newValue = Math.min(value + step, max);
        } else if (e.key === "ArrowLeft" || e.key === "ArrowDown") {
            newValue = Math.max(value - step, min);
        }
        if (newValue !== value && onChange) {
            onChange(newValue);
        }
    }, [value, step, min, max, onChange, disabled]);

    const handleFocus = useCallback(() => {
        setShowLabel(true);
    }, []);

    const handleBlur = useCallback(() => {
        setShowLabel(false);
    }, []);

    useEffect(() => {
        if (onChange) {
            onChange(value);
        }
    }, [value, onChange]);


    const sliderClasses = cn(
        st.Slider,
        {[st.SliderDragging]: isDragging},
        {[st.Sea]: accent === "sea"},
        {[st.Vertical]: orientation === "vertical"},
        {[st.Disabled]: disabled}
    );

    return (
        <div
            className={sliderClasses}
            ref={sliderRef}
            onClick={(e) => !disabled && handleMove(e)}
            onMouseDown={(e) => !disabled && handleStart(e)}
            onTouchStart={(e) => !disabled && handleStart(e)}
        >
            <span className={st.Rail}>
            </span>
            <span className={st.Track}
                  style={{[orientation === "horizontal" ? "width" : "height"]: `${position}%`}}>
            </span>
            <span className={st.Thumb}
                  style={{[orientation === "horizontal" ? "left" : "bottom"]: `${position}%`}}
                  tabIndex={disabled ? -1 : 0}
                  ref={thumbRef}
                  onKeyDown={handleKeyDown}
                  onFocus={handleFocus}
                  onBlur={handleBlur}
            >
                <span className={cn(st.Label, {[st.Label_show]: isShowLabel})}>
                    <span>{value}</span>
                </span>
                <input type="range"
                       min={min}
                       max={max}
                       step={step}
                       value={value}
                       onChange={(e) => onChange(Number(e.target.value))}
                       disabled={disabled}
                       hidden={true}
                />
            </span>
            {marksArray.map(mark => (
                <span
                    key={mark.value}
                    className={cn(st.Mark, {[st.MarkPassed]: mark.position <= position})}
                    style={{
                        [orientation === "horizontal" ? "left" : "bottom"]: `${mark.position}%`
                    }}
                />
            ))}
        </div>
    );
});

Slider.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    onChange: PropTypes.func,
    value: PropTypes.number,
    disabled: PropTypes.bool,
    accent: PropTypes.oneOf(["sea", "deep-purple"]),
    orientation: PropTypes.oneOf(["horizontal", "vertical"]),
    marks: PropTypes.bool
};

export default Slider;
