import React, {useEffect} from "react";
import {ErrorMessage, useField, useFormikContext} from "formik";
import cn from "classnames";
import PropTypes from "prop-types";
import st from "@ui/Select/Select.module.css";
import SelectDropdown from "@ui/Select/SelectDropdown.jsx";
import {SelectProvider, useSelectContext} from "@context/SelectContext/SelectContext.jsx";

//TODO Bad implementation, rewrite

const HiddenSelect = ({options, selectedValues, isMultiple, field}) => (
    <div tabIndex={-1} hidden={true} className={st.HiddenSelect}>
        <input
            type="text"
            tabIndex={-1}
            hidden={true}
            value={selectedValues.join(', ')}
            onChange={field.onChange}
            readOnly
        />
        <select
            multiple={isMultiple}
            tabIndex={-1}
            size={options.length}
            value={isMultiple ? selectedValues : selectedValues[0] || ''}
            onChange={() => {
            }}
        >
            {options.map((option, index) => (
                <option key={index} value={option.toString().toLocaleLowerCase()}>
                    {option}
                </option>
            ))}
        </select>
    </div>
);

HiddenSelect.propTypes = {
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedValues: PropTypes.arrayOf(PropTypes.string).isRequired,
    isMultiple: PropTypes.bool.isRequired,
    field: PropTypes.object.isRequired,
};

const Select = ({
                    options,
                    label,
                    name,
                    id,
                    isMultiple = true,
                    className,
                    size = "md",
                    rounded = "md",
                    ...props
                }) => {
    const [field, meta] = useField(name);
    const {
        isOpen,
        closeSelect,
        toggleSelect,
        selectedValues,
        isFocused,
        setIsFocused,
        selectRef,
        dropdownRef
    } = useSelectContext();

    useEffect(() => {
        const handleClickOutside = (e) => {
            if (!selectRef.current.contains(e.target) && !dropdownRef.current.contains(e.target)) {
                closeSelect();
            }
        };
        if (isOpen) {
            document.addEventListener('click', handleClickOutside);
            return () => document.removeEventListener('click', handleClickOutside);
        }
    }, [isOpen, closeSelect]);

    const selectClasses = cn(
        st.SelectedArea,
        {
            [st.Invalid]: meta.touched && meta.error && selectedValues.length === 0,
            [st.Size_sm]: size === "sm",
            [st.Size_md]: size === "md",
            [st.Size_lg]: size === "lg",
            [st.Rounded_xs]: rounded === "xs",
            [st.Rounded_sm]: rounded === "sm",
            [st.Rounded_md]: rounded === "md",
            [st.Rounded_lg]: rounded === "lg",
            [st.Rounded_xl]: rounded === "xl",
        },
        className,
    );

    return (
        <div className={st.Select} ref={selectRef}>
            <HiddenSelect
                options={options}
                field={field}
                selectedValues={selectedValues}
                isMultiple={isMultiple}
            />
            <button
                className={selectClasses}
                onClick={(e) => toggleSelect(e)}
                type="button"
                role="button"
                id={id}
                name={name}
                aria-haspopup="listbox"
                aria-expanded={isOpen}
                onFocus={() => setIsFocused(true)}
                onBlur={(e) => {
                    setIsFocused(false);
                    field.onBlur(e);
                }}
                data-open={isOpen}
                data-focus={isFocused}
                data-has-selected={selectedValues.length > 0}
                title={selectedValues.toString()}
                {...props}
            >
                <div className={st.SelectedValuesWrapper}>
                    <span className={st.SelectedValues}>
                        {selectedValues.join(', ')}
                    </span>
                </div>
                <label className={st.Label} htmlFor={id}>{label}</label>
                <svg className={st.Arrow} width="24" height="24" viewBox="0 0 24 24" stroke="currentColor"
                     strokeLinecap="round"
                     strokeLinejoin="round"
                     strokeWidth="2" fill="none" data-open={isOpen} xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9l6 6 6-6"/>
                </svg>
            </button>
            {isOpen && (

                <SelectDropdown/>
            )}

            <ErrorMessage name={name}>
                {msg =>
                    <div className={st.Error}>
                        <span className={st.ErrorMsg}>{msg}</span>
                    </div>
                }
            </ErrorMessage>
        </div>
    );
};

Select.propTypes = {
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    isMultiple: PropTypes.bool,
    className: PropTypes.string,
    size: PropTypes.oneOf(['sm', 'md', 'lg']),
    rounded: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
};

const SelectWrapper = (props) => {
    const {name, options, isMultiple} = props;
    const {setFieldValue} = useFormikContext();

    return (
        <SelectProvider options={options} isMultiple={isMultiple} name={name} setFieldValue={setFieldValue}>
            <Select {...props} />
        </SelectProvider>
    );
};

SelectWrapper.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    isMultiple: PropTypes.bool,
};

export default SelectWrapper;