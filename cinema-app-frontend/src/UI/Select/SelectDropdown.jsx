import React, {useEffect} from "react";
import st from "@ui/Select/Select.module.css";
import Portal from "@utils/Portal/Portal.jsx";
import {useSelectContext} from "@context/SelectContext/SelectContext.jsx";
import {useDropdownPosition} from "@hooks/UseDropdownPosition/useDropdownPosition.js";

const SelectDropdown = () => {
    const {
        options,
        itemRefs,
        highlightedIndex,
        setHighlightedIndex,
        selectedValues,
        handleSelectItem,
        handleNavigate,
        isClosing,
        isOpen,
        closeSelect,
        selectRef,
        dropdownRef,
    } = useSelectContext();
    const {dropdownPosition, isDropdownBelow} = useDropdownPosition(selectRef, dropdownRef, isOpen);

    const handleKeyDown = (e) => {
        switch (e.key) {
            case "Tab":
                closeSelect();
                break;
            case "ArrowUp":
            case 'ArrowLeft':
                e.preventDefault();
                handleNavigate('up');
                break;
            case 'ArrowDown':
            case 'ArrowRight':
                e.preventDefault();
                handleNavigate('down');
                break;
            case 'Enter':
                e.preventDefault();
                handleSelectItem(options[highlightedIndex]);
                break;
            case 'Escape':
                closeSelect();
                break;
            default:
                break;
        }
    };

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown);
        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [highlightedIndex, options, handleKeyDown]);

    return (

        <Portal containerElement={document.body}>
            <div
                style={{
                    position: 'absolute',
                    top: dropdownPosition.top,
                    left: dropdownPosition.left,
                    width: dropdownPosition.width,
                }}
                ref={dropdownRef}
                onMouseDown={(e) => e.preventDefault()}
            >
                <div
                    className={st.SelectArea}
                    data-hidden={isClosing}
                    data-placement={isDropdownBelow ? 'bottom' : 'top'}
                >
                    <div className={st.SelectArea_Wrapper}>
                        <ul className={st.SelectList}>
                            {options.map((option, index) => (
                                <li
                                    key={index}
                                    ref={(el) => {
                                        itemRefs.current[index] = el;
                                    }}
                                    className={st.SelectList_Item}
                                    onMouseEnter={() => {
                                        setHighlightedIndex(index);
                                    }}
                                    onMouseLeave={() => {
                                        setHighlightedIndex(null);
                                    }}
                                    data-selected={selectedValues.includes(option)}
                                    data-highlighted={highlightedIndex === index}
                                    onClick={() => handleSelectItem(option)}
                                    onMouseDown={(e) => e.preventDefault()}
                                >
                                    <div className={st.SelectItem}>
                                        <span className={st.SelectContent}>{option}</span>
                                        <span className={st.CheckWrapper}>
                                    <svg viewBox="0 0 17 18">
                                        <polyline
                                            fill="none"
                                            points="1 9 7 14 15 4"
                                            stroke="currentColor"
                                            strokeDasharray="22"
                                            strokeDashoffset={selectedValues.includes(option) ? "44" : "66"}
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth="1.5"
                                            style={{transition: "stroke-dashoffset .2s"}}
                                            data-selected={selectedValues.includes(option)}
                                        />
                                    </svg>
                                </span>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </Portal>
    );
};

export default SelectDropdown;