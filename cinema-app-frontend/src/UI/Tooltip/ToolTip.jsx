import React, {memo, useMemo, useState} from 'react';
import cn from "classnames";
import st from "@ui/Tooltip/ToolTip.module.css";

const Tooltip = memo(({
                          title,
                          children,
                          position = "right",
                          isDisabled,
                      }) => {
    const [isToolTipShow, setIsToolTipShow] = useState(false);
    const renderedChildren = useMemo(() => children, [children]);

    const tooltipClasses = cn(
        st.Tooltip,
        {
            [st.Top]: position === "top",
            [st.Bottom]: position === "bottom",
            [st.Left]: position === "left",
            [st.Right]: position === "right",
        },
    );

    const handleMouse = ( isEntering) => {
        setIsToolTipShow(isEntering);
    };

    return (
        <div
            className={tooltipClasses}
            onMouseEnter={() => handleMouse( true)}
            onMouseLeave={() => handleMouse( false)}
        >
            {renderedChildren}
            {isToolTipShow && !isDisabled &&
                <div className={st.Content}>
                    {title}
                </div>
            }
        </div>
    );
});

export default Tooltip;