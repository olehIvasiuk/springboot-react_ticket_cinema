import React from 'react';
import styles from  "./CubeLoading.module.css"

const CubeLoading = ({ text }) => {
    return (
        <div className={styles.Loader}>
            <div className={styles.Cube}>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div className="">{text}</div>
        </div>
    );
};

export default CubeLoading;
