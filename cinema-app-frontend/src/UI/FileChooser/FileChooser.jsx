import React, {useRef} from "react";
import PropTypes from "prop-types";
import Button from "@ui/Button/Button.jsx";

const FileChooser = ({
                         title,
                         wrapperClassName,
                         onFileUploaded,
                         accept = "all",
                         ...buttonProps
                     }) => {
    const inputRef = useRef(null);

    const handleButtonClick = () => {
        if (inputRef.current) {
            inputRef.current.click();
        }
    };

    const handleSelectedFile = (event) => {
        if (event && onFileUploaded) {
            const file = event.target.files[0];
            onFileUploaded(file);
        }
        if (inputRef) {
            inputRef.current.value = null;
        }
    };

    const acceptVariants = () => {
        switch (accept) {
            case "images":
                return ".jpeg, .jpg, .heic, .heif, .png, .gif, .webp, .svg";
            case "audio":
                return "audio/*";
            case "video":
                return "video/*";
            case "text":
                return "text/*";
            case "documents":
                return ".pdf, .doc, .docx, .ppt, .pptx, .xls, .xlsx";
            case "archives":
                return ".zip, .rar, .tar, .gzip";
            default:
                return "*";
        }
    };

    return (
        <div style={{display: "contents"}}>
            <Button
                onClick={handleButtonClick}
                {...buttonProps}
            >
                {title}
            </Button>
            <input
                ref={inputRef}
                hidden={true}
                type="file"
                name="coverFile"
                accept={acceptVariants()}
                onChange={handleSelectedFile}
            />
        </div>
    );
};

FileChooser.propTypes = {
    title: PropTypes.string.isRequired,
    onFileUploaded: PropTypes.func.isRequired,
    wrapperClassName: PropTypes.string,
    accept: PropTypes.oneOf(["all", "images", "audio", "video", "text", "documents", "archives"]),
    className: PropTypes.string,
    size: PropTypes.oneOf(["sm", "md", "lg"]),
    rounded: PropTypes.oneOf(["sm", "md", "lg", "full", "none"]),
    color: PropTypes.oneOf(["default", "primary", "secondary"]),
    variant: PropTypes.oneOf(["solid", "outline", "ghost"]),
    isIconOnly: PropTypes.bool,
    iconId: PropTypes.string,
    iconX: PropTypes.number,
    iconY: PropTypes.number,
    isLoading: PropTypes.bool,
    isDisabled: PropTypes.bool,
    type: PropTypes.oneOf(["button", "submit", "reset"]),
    role: PropTypes.string,
    isHidden: PropTypes.bool,
    tabIndex: PropTypes.number,
    ariaLabel: PropTypes.string,
};

export default FileChooser;
