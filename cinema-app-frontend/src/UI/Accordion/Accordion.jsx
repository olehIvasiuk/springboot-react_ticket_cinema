import React, {useEffect, useRef} from "react";
import cn from "classnames";
import Icon from "@ui/Icon/Icon.jsx";
import st from "@ui/Accordion/Accordion.module.css"

const Accordion = ({title, iconId, children, isOpen, onToggle, iconX, iconY}) => {
    const contentRef = useRef(null);

    useEffect(() => {
        if (isOpen) {
            contentRef.current.style.maxHeight = contentRef.current.scrollHeight + "px";
        } else {
            contentRef.current.style.maxHeight = "0px";
        }
    }, [isOpen]);

    return (
        <div className={cn(st.Accordion, {[st.Accordion_show]: isOpen})}>
            <button
                className={cn(st.Header, "act")}
                onClick={onToggle}
                tabIndex={0}
                role="button"
            >
                {iconId && (
                    <Icon iconId={iconId} iconY={iconY} iconX={iconX}/>
                )}
                {title}
            </button>
            <div ref={contentRef} className={st.Body}>
                <div className={st.Content}>
                    {React.Children.map(children, (child, index) => {
                        return React.cloneElement(child, {
                            tabIndex: isOpen ? 0 : -1
                        });
                    })}
                </div>
            </div>
        </div>
    );
};

export default Accordion;