/**
 * A customizable input component that supports various input types, sizes, and configurations.
 *
 * @param {Object} props - The props for the component.
 * @param {string} props.label - The label for the input field.
 * @param {string} props.name - The name of the input field, used for form management.
 * @param {string} props.id - The ID of the input field.
 * @param {'email'|'number'|'password'|'search'|'tel'|'text'|'url'} [props.type='text'] - The type of input field.
 * @param {'decimal'|'email'|'numeric'|'search'|'tel'|'text'|'url'} [props.inputMode='text'] - The input mode for mobile keyboards.
 * @param {string} [props.placeholder] - The placeholder text for the input field.
 * @param {string} [props.autoComplete='off'] - The autocomplete attribute for the input field.
 * @param {boolean} [props.isReadOnly=false] - Whether the input field is read-only.
 * @param {boolean} [props.isDisabled=false] - Whether the input field is disabled.
 * @param {boolean} [props.symbolCounter=false] - Whether to show a symbol counter.
 * @param {'sm'|'md'|'lg'} [props.size='md'] - The size of the input field.
 * @param {'sm'|'md'|'lg'|'xl'} [props.rounded='md'] - The border-radius of the input field.
 * @param {string} [props.className] - Additional class names to apply to the input field.
 * @param {number} [props.maxLength] - The maximum number of characters allowed in the input field.
 * @param {string} [props.description] - A description text displayed below the input field.
 * @param {'input'|'textarea'} [props.as='input'] - The HTML element type to render, either an input or textarea.
 * @returns {JSX.Element} The rendered Input component.
 */

import React, {useState} from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import st from "@ui/Input/Input.module.css";
import {ErrorMessage, Field, useField} from "formik";

const Input = ({
                   label,
                   name,
                   id,
                   type = "text",
                   inputMode = "text",
                   placeholder,
                   autoComplete = "off",
                   isReadOnly = false,
                   isDisabled = false,
                   symbolCounter = false,
                   size = "md",
                   rounded = "md",
                   className,
                   maxLength,
                   description,
                   as = "input",
                   ...props
               }) => {
    const [field, meta, helpers] = useField(name);
    const [isFocused, setIsFocused] = useState(false);
    const [hasValue, setHasValue] = useState(false);
    const countSymbols = field.value.length;

    const inputClasses = cn(
        st.TextInput,
        {[st.TextArea]: as === "textarea"},
        {
            [st.Invalid]: meta.touched && meta.error,
        },
        {
            [st.Size_sm]: size === "sm",
            [st.Size_md]: size === "md",
            [st.Size_lg]: size === "lg",
        },
        {
            [st.Rounded_sm]: rounded === "sm",
            [st.Rounded_md]: rounded === "md",
            [st.Rounded_lg]: rounded === "lg",
            [st.Rounded_xl]: rounded === "xl",
        },
        className,
    );

    const handleChange = (e) => {
        const {value} = e.target;

        setHasValue(!!e.target.value);
        e.target.value = maxLength ? value.slice(0, maxLength) : value;
        field.onChange(e);
    };

    return (
        <div className={st.TextField} data-focus={isFocused} data-has-value={hasValue}>
            <div className={st.InputWrapper}>
                <Field
                    {...field}
                    as={as}
                    id={id}
                    name={name}
                    type={type}
                    inputMode={inputMode}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    autoComplete={autoComplete}
                    onFocus={() => setIsFocused(true)}
                    onBlur={(e) => {
                        setIsFocused(false);
                        field.onBlur(e);
                    }}
                    onChange={handleChange}
                    className={inputClasses}
                    readOnly={isReadOnly}
                    disabled={isDisabled}
                    aria-invalid={meta.touched && meta.error ? "true" : "false"}
                    aria-describedby={meta.touched && meta.error ? `${id}-error` : null}
                    {...props}
                />
                <label className={st.Label} htmlFor={id}>{label}</label>
            </div>
            <ErrorMessage name={name}>
                {msg =>
                    <div className={st.Error}>
                        <span className={st.ErrorMsg}>{msg}</span>
                    </div>
                }
            </ErrorMessage>
            {description &&
                <div className={st.Description}>
                    <div className={st.DescriptionArea}>
                        <p className={st.DescriptionContent}>{description}</p>
                    </div>
                </div>
            }
            {symbolCounter &&
                <div className={st.Description}>
                    <div className={st.DescriptionArea}>
                        <p className={st.DescriptionContent}>{`${countSymbols}/${maxLength} символів введено`}</p>
                    </div>
                </div>
            }
        </div>
    );
};

Input.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    type: PropTypes.oneOf(["email", "number", "password", "search", "tel", "text", "url"]),
    inputMode: PropTypes.oneOf(["decimal", "email", "numeric", "search", "tel", "text", "url"]),
    placeholder: PropTypes.string,
    autoComplete: PropTypes.string,
    isReadOnly: PropTypes.bool,
    isDisabled: PropTypes.bool,
    size: PropTypes.oneOf(["sm", "md", "lg"]),
    rounded: PropTypes.oneOf(["sm", "md", "lg", "xl"]),
    className: PropTypes.string,
    maxLength: PropTypes.number,
    symbolCounter: PropTypes.bool,
    description: PropTypes.string,
    as: PropTypes.oneOf(["input", "textarea"]),
};

export default Input;