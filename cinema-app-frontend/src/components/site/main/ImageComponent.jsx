import React from 'react';

const ImageComponent = ({ imagePath }) => {
    return (
        <img src={`http://localhost:8080/${imagePath}`} alt="Movie Poster" />
    );
};

export default ImageComponent;