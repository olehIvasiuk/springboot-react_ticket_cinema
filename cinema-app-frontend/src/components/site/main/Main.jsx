import React from 'react';
import Header from "../header/Header.jsx";
import SectionMain from "./MainContent.jsx";
import Footer from "../footer/Footer.jsx";

const Main = () => {
    return (
        <div className='tt_root-wrapper'>
            <Header/>
            <SectionMain/>
            <Footer/>
        </div>
    );
};

export default Main;