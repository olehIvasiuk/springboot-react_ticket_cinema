import React, {useCallback, useEffect, useRef, useState} from 'react';
import FocusTrap from 'focus-trap-react';
import SearchService from '../../../services/SearchService.js';
import './SearchModal.css';
import SearchHeader from './SearchHeader';
import SearchFooter from './SearchFooter';
import Separator from "../separator/Separator.jsx";
import SearchContent from "./SearchContent.jsx";

const SearchModal = ({onClose}) => {
    const ref = useRef(null);
    const [inpVal, setInputValue] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isVisible, setIsVisible] = useState(true);

    const handleInpChange = async (event) => {
        const {value} = event.target;
        setInputValue(value);

        if (value.trim() === '') {
            setSearchResults([]);
            return;
        }

        setIsLoading(true);

        try {
            const results = await SearchService.searchMovies(value);
            setSearchResults(results);
        } catch (err) {
            console.log('data error');
        } finally {
            setIsLoading(false);
        }
    };

    const handleReset = () => {
        setInputValue('');
        setSearchResults([]);
    };

    const handleClickOutside = (event) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setIsVisible(false);
            setTimeout(() => onClose(), 150);
        }
    };

    const closeModal = (event) => {
        setIsVisible(false);
        setTimeout(() => onClose(), 150);
    };

    const handleKeyDown = useCallback((event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
        } else if (event.key === 'Escape') {
            setIsVisible(false);
            setTimeout(() => onClose(), 150);
        }
    }, [onClose]);

    useEffect(() => {
        document.body.style.overflow = 'hidden';
        document.addEventListener('mousedown', handleClickOutside);
        document.addEventListener('keydown', handleKeyDown);
        return () => {
            document.body.style.overflow = '';
            document.removeEventListener('mousedown', handleClickOutside);
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [handleClickOutside, handleKeyDown]);

    return (
        <div className="tt-search">
            <FocusTrap active={isVisible}>
                <div ref={ref} className={`tt-search-container ${isVisible ? 'show' : 'hide'}`}>
                    <SearchHeader inpVal={inpVal} handleInpChange={handleInpChange} handleReset={handleReset}
                                  closeModal={closeModal}/>
                    <SearchContent inpVal={inpVal} searchResults={searchResults} isLoading={isLoading}/>
                    <Separator bg={'--zinc-700'}/>
                    <SearchFooter searchResults={searchResults}/>
                </div>
            </FocusTrap>
        </div>
    );
};

export default SearchModal;