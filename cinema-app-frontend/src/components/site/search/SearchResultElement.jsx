import React, {useState} from 'react';

const SearchResultElement = ({onLoad, movieName, imageName, allPreLoaded}) => {
    const [imageLoaded, setImageLoaded] = useState(false);

    const handleLoad = () => {
        setImageLoaded(true);
        if (onLoad) {
            onLoad();
        }
    };

    const showContent = allPreLoaded && imageLoaded;

    return (
        <>
            <div className={`search-movie__thumb ${showContent ? '' : 'hidden'}`}>
                <img
                    src={`http://localhost:8080/api/images/uploads/${imageName}`}
                    className={`search-movie__thumb_img ${showContent ? '' : 'hidden'}`}
                    alt={movieName}
                    onLoad={handleLoad}
                    onError={handleLoad}
                    decoding={"async"}
                />
            </div>
            <div className={`search-movie__title ${showContent ? '' : 'hidden'}`}>
                <span>{movieName}</span>
            </div>
        </>
    );
};

export default SearchResultElement;
