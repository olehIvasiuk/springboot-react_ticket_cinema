import React, { useState, useEffect } from 'react';
import SearchResultElement from './SearchResultElement';
import Skeleton from "../skeleton/Skeleton.jsx";

const SearchTipDefault = React.memo(() => (
    <div className="search-results__write">
        <span className="search-results__greeting-tip">Знайдеться все (ну майже)</span>
    </div>
));

const SearchTipNothingFound = React.memo(() => (
    <div className="search-results__no-results">
        <svg width="64" height="64">
            <use href="src/assets/icons/svg/cite-sprites.svg#icon-search-fail"></use>
        </svg>
        <span>Ваш запит не дав результатів</span>
    </div>
));

const SearchContent = ({ inpVal, searchResults, isLoading }) => {
    const [loadedImagesCount, setLoadedImagesCount] = useState(0);

    useEffect(() => {
        setLoadedImagesCount(0);
    }, [searchResults]);

    const handleImageLoad = () => {
        setLoadedImagesCount(prevCount => prevCount + 1);
    };

    const allImagesLoaded = loadedImagesCount === searchResults.length && searchResults.length > 0;

    return (
        <div className="search-results__area">
            <div className="search-results__tip">
                {inpVal.length === 0 && <SearchTipDefault />}
                {searchResults.length === 0 && inpVal.length > 0 && !isLoading && <SearchTipNothingFound />}
            </div>

            <ul className="search-results__list">
                {searchResults.map(value => (
                    <li key={value.id} className={`search-result__list-item`}>
                        <a href={'/movie/' + value.id} className="search-result__element">
                            <SearchResultElement
                                movieName={value.movieName}
                                imageName={value.coverPath}
                                onLoad={handleImageLoad}
                                allPreLoaded={allImagesLoaded}
                            />
                            {!allImagesLoaded && <Skeleton/>}
                        </a>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default SearchContent;