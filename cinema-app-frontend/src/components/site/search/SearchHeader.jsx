import React from 'react';

const SearchHeader = ({inpVal, handleInpChange, handleReset, closeModal}) => (
    <header className="search-header">
        <form className="search-form">
            <div className="search-form__container">
                <label className="search-label" htmlFor="search-input">
                    <svg width="24" height="24">
                        <use href="src/assets/icons/svg/cite-sprites.svg#icon-search"></use>
                    </svg>
                </label>
                <input
                    className="search-input"
                    id="search-input"
                    autoComplete="off"
                    autoCorrect="off"
                    autoCapitalize="off"
                    spellCheck="false"
                    placeholder="Я шукаю..."
                    tabIndex={1}
                    maxLength="100"
                    name="search-input"
                    type="search"
                    value={inpVal}
                    onChange={handleInpChange}
                />
                {inpVal.length > 0 && (
                    <button
                        type="button"
                        title="Очистити ввід"
                        className="search-reset"
                        onClick={handleReset}
                    >
                        <svg width="24" height="24">
                            <use href="src/assets/icons/svg/cite-sprites.svg#icon-red-cross"></use>
                        </svg>
                    </button>
                )}
            </div>
        </form>
        <button className="search-cancel"
                onClick={closeModal}
        >
            <span>Назад</span>
        </button>
    </header>
);

export default SearchHeader;