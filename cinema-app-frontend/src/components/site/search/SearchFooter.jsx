import React from 'react';

const SearchFooter = ({ searchResults }) => (
    <footer className="search-footer__tips">
        <kbd className="search-footer__functional-keys">Esc</kbd>
        {searchResults.length > 0 && (
            <div className="search-results__write">
                <span className="search-results__quantity">Знайдено фільмів: {searchResults.length}</span>
            </div>
        )}
        <kbd className="search-footer__functional-keys">Tab</kbd>
    </footer>
);

export default SearchFooter;