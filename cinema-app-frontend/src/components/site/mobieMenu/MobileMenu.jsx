import React, {useEffect} from 'react';
import MobMenuNav from "./sections/MobMenuNav.jsx";
import "./mobile-menu.css"
import MobMenuHeader from "./sections/MobMenuHeader.jsx";


const MobileMenu = ({isHide}) => {

    useEffect(() => {
        if (isHide) {
            document.body.style.overflow = 'hidden';
        } else {
            document.body.style.overflow = '';
        }
        return () => {
            document.body.style.overflow = '';
        };
    }, [isHide]);


    return (
        <div className="tt-mobile-menu">
            <div className={`tt-mobile-menu__container ${isHide ? 'open' : 'hide'}`}>
                <div className="tt-mobile-menu__content">
                    <MobMenuHeader/>
                    <MobMenuNav/>
                    <div className="mobile-menu__footer"></div>
                </div>
            </div>
        </div>
    );
};

export default MobileMenu;