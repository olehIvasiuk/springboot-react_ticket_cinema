import React, {useState} from 'react';
import Separator from "../../separator/Separator.jsx";
import AccordionItem from "../../accordion/AccordionItem.jsx";
import Link from "../../a-button/Link.jsx";

const MobMenuNav = () => {
    const [openIndex, setOpenIndex] = useState(null);

    const toggleAccordion = (index) => {
        setOpenIndex(openIndex === index ? null : index);
    };


    return (
        <nav className="mobile-menu__nav">
            <Separator text={'Меню сайту'} color={'--zinc-400'} bg={'--zinc-500'} mb={'.375'} mt={'.75'}/>
            <div className="mobile-menu__nav-container">
                <Link classWrapper={"mobile-menu__nav-link__container"}
                      className={"mobile-menu__nav__link schedule act"}
                      iconId={"icon-nav-schedule"}
                      href={"#"}
                      text={"Розклад сеансів"}
                />
                <Separator bg={'--blue-400'} />
                <AccordionItem
                    title="Фільми"
                    isOpen={openIndex === 0}
                    onToggle={() => toggleAccordion(0)}
                    iconId={"icon-nav-movies"}
                    iconX={24}
                    iconY={24}
                >
                    <a href="#">Скоро на екранах</a>
                    <Separator bg={"--zinc-600"} height={"2"}/>
                    <a href="#">Архів</a>
                </AccordionItem>
                <Link classWrapper={"mobile-menu__nav-link__container"}
                      className={"mobile-menu__nav__link act"}
                      iconId={"icon-nav-theaters"}
                      href={"#"}
                      text={"Кінотеатри"}
                />
                <Link classWrapper={"mobile-menu__nav-link__container"}
                      className={"mobile-menu__nav__link act"}
                      iconId={"icon-nav-news"}
                      href={"/control-panel"}
                      text={"Новини"}
                />
                <AccordionItem
                    title="Інше"
                    isOpen={openIndex === 1}
                    onToggle={() => toggleAccordion(1)}
                    iconId={"icon-nav-route-other"}
                    iconX={24}
                    iconY={24}
                >
                    <a href="#">Повернення квитків</a>
                    <Separator bg={"--zinc-600"} height={"2"}/>
                    <a href="#">Допомога</a>
                    <Separator bg={"--zinc-600"} height={"2"}/>
                    <a href="#">Про Ticket Cinema</a>
                </AccordionItem>
            </div>
        </nav>
    );
};

export default MobMenuNav;