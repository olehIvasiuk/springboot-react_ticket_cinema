import React from 'react';
import Separator from "../../separator/Separator.jsx";


const MobMenuLocation = () => {

    return (
        <div className="mobile-menu__header-location">
            <a className="mobile-menu__location-link btn" tabIndex={0}>
                <span>м. Дніпро</span>
            </a>
        </div>
    );
};

const MobMenuUserSection = () => {
    return (
        <div className="mobile-menu__header-user__section">
            <ul className="mobile-menu__user-section__list">
                <li className="mobile-menu__user-section__list-item">
                    <a href="" className="mobile-menu__user-section__link act">
                        <span>Квитки</span>
                    </a>
                </li>
                <li className="mobile-menu__user-section__list-item">
                    <a href="" className="mobile-menu__user-section__link act">
                        <span>Кабінет</span>
                    </a>
                </li>
            </ul>
        </div>
    );
};

const MobMenuHeader = () => {
    return (
        <div className="mobile-menu__content-header">
            <Separator text={'Розташування'} color={'--zinc-400'} bg={'--zinc-500'} mtb={".375"}/>
            <MobMenuLocation/>
            <Separator text={'Юзер'} color={'--zinc-400'} bg={'--zinc-500'} mt={"1"} mb={'.375'}/>
            <MobMenuUserSection/>
        </div>
    );
};

export default MobMenuHeader;