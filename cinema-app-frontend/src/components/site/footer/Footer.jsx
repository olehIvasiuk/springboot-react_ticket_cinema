import React from "react";

export default function Footer() {
  return (
    <footer className="tt-footer">
        <div className="tt-footer__container">
          <div className="tt-footer__desc">
            <ul className="tt-footer__desc-list">
              <li className="tt-footer__desc-list--item">
                <h3 className="tt-footer__desc-cite_name">Ticket UA</h3></li>
              <li className="tt-footer__desc-list--item">
                <span className="tt-footer__desc-sub_desc">Сервіс онлайн купівлі квитків у кінотеатри України ticket.com.ua</span></li>
              <li className="tt-footer__desc-list--item">
                <span className="tt-footer__desc-sub_desc">Copyright ©2023-2024 &#8226; All Rights Reserved</span></li>
            </ul>
          </div>
          <div className="tt-footer__soc">
            <div className="tt-footer__soc-container">
              <div className="footer-soc">
                <div className="footer-soc__head">
                  <span className="footer-soc__head-content">Ми в соціальних мережах</span>
                </div>
                <ul className="footer-soc__list">
                  <li className="footer-soc__list-item instagram__btn">
                    <a href="#">
                      <svg className="cp_quick-action--svg"  width="24" height="24">
                        <use href="src/assets/icons/svg/cite-sprites.svg#icon-footer_soc-in"></use>
                      </svg>
                    </a>
                  </li>
                  <li className="footer-soc__list-item facebook__btn">
                    <a href="#">
                      <svg className="cp_quick-action--svg"  width="24" height="24">
                        <use href="src/assets/icons/svg/cite-sprites.svg#icon-footer_soc-fb"></use>
                      </svg>
                    </a>
                  </li>
                  <li className="footer-soc__list-item youtube__btn">
                    <a href="#">
                      <svg className="cp_quick-action--svg"  width="24" height="24">
                        <use href="src/assets/icons/svg/cite-sprites.svg#icon-footer_soc-yt"></use>
                      </svg>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="footer-tg-bot">
                <div className="footer-tg-bot__head">
                  <span className="footer-tg-bot__head-content">Купуй квитки через Telegram бота</span>
                </div>
                <ul className="footer-tg-bot__list">
                  <li className="footer-tg-bot__list-item telegram-btn">
                    <a href="#">
                      <svg className="cp_quick-action--svg"  width="24" height="24">
                          <use href="src/assets/icons/svg/cite-sprites.svg#icon-footer_soc-tg"></use>
                      </svg>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="tt-footer__download-apps">
            <div className="download-apps__head">
              <span className="download-apps__head-content">Наші додатки</span>
            </div>
            <ul className="download-apps__list">
              <li className="download-apps__list-item">
                <a href="#" className="download-apps__list-item--link">
                  <img src="src/assets/img/appstoreBtn.png" alt="AppStore__icon" />
                </a>
              </li>
              <li className="download-apps__list-item">
                <a href="#" className="download-apps__list-item--link">
                  <img src="src/assets/img/googlePlayBtn.png" width="200" height="40" alt="Google-play__icon"/>
                </a>
              </li>
            </ul>
          </div>
        </div>
    </footer>
  );
}
