import React from 'react';

const HeaderLocation = () => {
    return (
        <div className="tt-header__location">
            <label className="header-location__label" htmlFor="hid-location__btn">місто</label>
            <button className="header-location__btn btn" id="hid-location__btn">Дніпро</button>
        </div>
    );
};

export default HeaderLocation;