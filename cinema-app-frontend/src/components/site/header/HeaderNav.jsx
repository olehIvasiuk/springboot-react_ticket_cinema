import React from "react";

export default function HeaderNav() {
  return (
    <nav className="tt-header__nav">
      <div className="tt-header__nav-container" id="hid-header-nav">
        <ul className="header-nav__list" id="hid-header__list">
          <li className="header-nav__list-item">
            <a className="header-nav__link btn primary" href="#">Розклад</a>
          </li>
          <li className="header-nav__list-item">
            <a className="header-nav__link" href="/movie-info">Фільми</a>
          </li>
          <li className="header-nav__list-item">
            <a className="header-nav__link" href="#">Кінотеатри</a>
          </li>
          <li className="header-nav__list-item">
            <a className="header-nav__link" href="/control-panel">Новини</a>
          </li>
          <li className="header-nav__list-item">
            <a className="header-nav__link" href="/movie-create">Інше</a>
          </li>
        </ul>
      </div>
    </nav>
  );
}
