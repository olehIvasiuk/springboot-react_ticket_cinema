import React from 'react';

const HeaderUserMng = () => {
    return (
        <div className="tt-header__user-manager" id="hid-user-manager">
            <ul className="header-user-manager__list" id="hid-user-manager__list">
                <li className="header-user-manager__list-item">
                    <a href="#" className="header-user-manager__link" id="hid-tickets__link">
                        <svg className="cp_quick-action--svg" width="34" height="34">
                            <use href="src/assets/icons/svg/cite-sprites.svg#icon-header__tickets"></use>
                        </svg>
                    </a>
                    <span className="header-user-manager__text-content hidden">Квитки</span>
                </li>
                <li className="header-user-manager__list-item">
                    <a href="#" className="header-user-manager__link" id="hid-account__link">
                        <svg className="cp_quick-action--svg" width="34" height="34">
                            <use
                                href="src/assets/icons/svg/cite-sprites.svg#icon-header__user-cabinet"></use>
                        </svg>
                    </a>
                    <span className="header-user-manager__text-content hidden">Увійти</span>
                </li>
            </ul>
        </div>
    );
};

export default HeaderUserMng;