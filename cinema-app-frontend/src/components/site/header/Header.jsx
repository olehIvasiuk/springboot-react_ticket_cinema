import React, { useState, useEffect, useRef } from 'react';
import HeaderNav from './HeaderNav';
import SearchComponent from '../search/SearchComponent';
import HeaderSearch from './HeaderSearch';
import HeaderLocation from './HeaderLocation';
import HeaderUserMng from './HeaderUserMng';
import HeaderLogo from './HeaderLogo';
import BurgerMenu from '../header-burger/BurgerMenu.jsx';
import MobileMenu from "../mobieMenu/MobileMenu.jsx";

const Header = () => {
    const [isSearchVisible, setSearchVisible] = useState(false);
    const [isMenuOpen, setMenuOpen] = useState(false);
    const [isMenuHidden, setMenuHidden] = useState(false);
    const headerRef = useRef(null);

    useEffect(() => {
        const handleFocusTrap = (event) => {
            if (headerRef.current && !headerRef.current.contains(event.target)) {
                event.stopPropagation();
                headerRef.current.focus();
            }
        };

        if (isMenuOpen) {
            document.addEventListener('focus', handleFocusTrap, true);
        } else {
            document.removeEventListener('focus', handleFocusTrap, true);
        }

        return () => {
            document.removeEventListener('focus', handleFocusTrap, true);
        };
    }, [isMenuOpen]);

    const toggleSearchModal = () => {
        if (isMenuOpen) {
            setMenuOpen(false);
            setMenuHidden(false);
        }
        setSearchVisible(!isSearchVisible);
    };

    const closeSearchComponent = () => {
        setSearchVisible(false);
    };

    const toggleMenu = () => {
        if (isSearchVisible) {
            setSearchVisible(false);
        }
        setMenuHidden(!isMenuHidden);
        setTimeout(() => setMenuOpen(!isMenuOpen), 150);
    };

    return (
        <header className="tt-header__wrapper" ref={headerRef} tabIndex={-1}>
            <div className="tt-header__container">
                <div className="tt-header__left-group" id="hid-left-group">
                    <HeaderLogo />
                    <HeaderNav />
                </div>
                <div className="tt-header__right-group">
                    <HeaderSearch toggleSearchModal={toggleSearchModal} />
                    <HeaderLocation />
                    <HeaderUserMng />
                    <BurgerMenu isOpen={isMenuOpen} toggleMenu={toggleMenu} mode={"hiding"}/>
                </div>
            </div>

            {isSearchVisible && <SearchComponent onClose={closeSearchComponent} />}
            {isMenuOpen && <MobileMenu isHide={isMenuHidden} />}
        </header>
    );
};

export default Header;