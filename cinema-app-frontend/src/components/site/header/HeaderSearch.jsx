import React from "react";

export default function HeaderSearch({ toggleSearchModal }) {
    return (
        <div className="tt-header__search">
            <button className="header__search-btn btn"
                    type="button"
                    onClick={toggleSearchModal}
            >
                <svg width={24} height={24}>
                    <use xlinkHref="src/assets/icons/svg/cite-sprites.svg#icon-search"></use>
                </svg>
                <span className="header__search-text">Пошук</span>
            </button>
            <button className="header__search-compact__btn btn"
                    onClick={toggleSearchModal}
            >
                <svg width={24} height={24}>
                    <use xlinkHref="src/assets/icons/svg/cite-sprites.svg#icon-search"></use>
                </svg>
            </button>
        </div>
    );
}
