import React from 'react';
import './BurgerMenu.css';

const BurgerMenu = ({ isOpen, toggleMenu, text, mode = ""}) => {
    return (
        <div className={`tt_header-mob-menu__burger ${mode}`}>
            <button className={`act mob-menu__toggle ${isOpen ? 'open' : ''}`} onClick={toggleMenu}>
                <div className="burger-box">
                    <div className="burger-line"></div>
                </div>
                {text && <span>{text}</span>}
            </button>
        </div>
    );
};

export default BurgerMenu;