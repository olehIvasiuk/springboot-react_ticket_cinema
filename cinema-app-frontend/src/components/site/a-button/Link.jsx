import React from 'react';

const Link = ({href, iconId, text, className, classWrapper}) => {
    return (
        <div className={classWrapper}>
            <a className={className} href={href}>
                <svg width={24} height={24}>
                    <use xlinkHref={`src/assets/icons/svg/cite-sprites.svg#${iconId}`}></use>
                </svg>
                <span>{text}</span>
            </a>
        </div>
    );
};

export default Link;