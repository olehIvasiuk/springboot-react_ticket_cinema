import React from 'react';
import './skeleton.css'

const Skeleton = ({classes}) => {

    return (
        <>
            <div className="skeleton-container">
                <div className="skeleton-circle animation-pulse"></div>
                <div className="skeleton-title animation-pulse"></div>
            </div>
        </>
    );
};

export default Skeleton;