import React, {memo} from 'react';

const DateFromTo = memo(({ titleFrom, titleTo, nameFrom, nameTo, idFrom, idTo, isRequired = true, fromValue, toValue, onFromChange, onToChange, errorFrom, errorTo }) => {
    return (
        <div className="rental-period-block">
            <div className={`tt_date-field ${errorFrom ? 'invalid' : ''}`}>
                <div className="tt_date-field__input-area">
                    <input
                        className="tt_date-field__input"
                        type="date"
                        id={idFrom}
                        name={nameFrom}
                        placeholder=""
                        required={isRequired}
                        autoComplete="off"
                        value={fromValue}
                        onChange={onFromChange}
                        max={toValue ? toValue : undefined}
                    />
                    <label className="tt_field__title" htmlFor={idFrom}>{titleFrom}</label>
                </div>
            </div>
            <div className={`tt_date-field ${errorTo ? 'invalid' : ''}`}>
                <div className="tt_date-field__input-area">
                    <input
                        className="tt_date-field__input"
                        type="date"
                        id={idTo}
                        name={nameTo}
                        placeholder=""
                        required={isRequired}
                        autoComplete="off"
                        value={toValue}
                        onChange={onToChange}
                        min={fromValue ? fromValue : undefined}
                    />
                    <label className="tt_field__title" htmlFor={idTo}>{titleTo}</label>
                </div>
            </div>
        </div>
    );
});

export default DateFromTo;































































// const DateFromTo = ({ titleFrom, titleTo, nameFrom, nameTo, idFrom, idTo, isRequired = true }) => {
//     const [fromDate, setFromDate] = useState('');
//     const [toDate, setToDate] = useState('');
//
//     const handleFromDateChange = (e) => {
//         const newFromDate = e.target.value;
//         setFromDate(newFromDate);
//         if (newFromDate && toDate && new Date(newFromDate) > new Date(toDate)) {
//             setToDate('');
//         }
//     };
//
//     const handleToDateChange = (e) => {
//         const newToDate = e.target.value;
//         setToDate(newToDate);
//         if (newToDate && fromDate && new Date(newToDate) < new Date(fromDate)) {
//             setFromDate('');
//         }
//     };
//
//     return (
//         <div className="rental-period-block">
//             <div className="tt_date-field">
//                 <div className="tt_date-field__input-area">
//                     <input
//                         className="tt_date-field__input"
//                         type="date"
//                         id={idFrom}
//                         name={nameFrom}
//                         placeholder=""
//                         required={isRequired}
//                         autoComplete="off"
//                         value={fromDate}
//                         onChange={handleFromDateChange}
//                         max={toDate ? toDate : undefined}
//                     />
//                     <label className="tt_field__title" htmlFor={idFrom}>{titleFrom}</label>
//                 </div>
//             </div>
//             <div className="tt_date-field">
//                 <div className="tt_date-field__input-area">
//                     <input
//                         className="tt_date-field__input"
//                         type="date"
//                         id={idTo}
//                         name={nameTo}
//                         placeholder=""
//                         required={isRequired}
//                         autoComplete="off"
//                         value={toDate}
//                         onChange={handleToDateChange}
//                         min={fromDate ? fromDate : undefined}
//                     />
//                     <label className="tt_field__title" htmlFor={idTo}>{titleTo}</label>
//                 </div>
//             </div>
//         </div>
//     );
// };
//
// export default DateFromTo;