import React, {useState, memo} from 'react';
import cn from "classnames";
import {Link} from "react-router-dom";
import Icon from "@ui/Icon/Icon.jsx";
import Button from "@ui/Button/Button.jsx";
import Tooltip from "@ui/Tooltip/ToolTip.jsx";
import Separator from "@ui/Separator/Separator.jsx";
import st from "@components/control_panel/aside-menu/AsideMenu.module.css";

//TODO Completely redesign this component

const MenuAccordionItem = memo(({
                                    label,
                                    children,
                                    iconId,
                                    iconY = 32,
                                    iconX = 32,
                                    isExpanded,
                                    toolTip,
                                    position = "right",
                                }) => {

    const [open, setOpen] = useState(false);

    return (
        <div
            className={cn(st.MenuItem__Accordion, {[st.Open]: open})}
        >
            <Tooltip title={toolTip} position={position} isDisabled={isExpanded}>
                <button
                    className={cn(st.AccordionButton, "act")}
                    onClick={() => setOpen(!open)}
                    tabIndex={0}
                    role="button"
                >
                    <Icon iconId={iconId} iconY={iconY} iconX={iconX}/>
                    <span className={st.AccordionLabel}>{label}</span>
                </button>
            </Tooltip>
            <div className={cn(st.AccordionContent, {[st.Open]: open})}>
                {React.Children.map(children, (child) =>
                    React.cloneElement(child, {tabIndex: open ? 0 : -1}))}
            </div>
        </div>);
});

const MenuAccordionSubItem = memo(({
                                       label, iconId, iconY = 24, iconX = 24,
                                       isExpanded, toolTip, position = "right", tabIndex, link, isSelected, onClick
                                   }) => {

    return (
        <div className={st.Submenu__item}>
            <Tooltip title={toolTip} position={position} isDisabled={isExpanded}>
                <Link
                    className={cn(st.Submenu__link, {[st.Selected]: isSelected})}
                    to={link}
                    onClick={onClick}
                    tabIndex={tabIndex}
                >
                    <Icon iconId={iconId} iconY={iconY} iconX={iconX}/>
                    <span className={st.Submenu__label}>{label}</span>
                </Link>
            </Tooltip>
        </div>
    );
});

const MenuItem = memo(({
                           label,
                           isExpanded,
                           iconId,
                           iconX = 32,
                           iconY = 32,
                           link
                       }) => {
    return (
        <Tooltip title={label} isDisabled={isExpanded}>
            <Link className={cn(st.MenuItem__link, "act")}
                  to={link}
            >
                <Icon iconId={iconId} iconY={iconY} iconX={iconX}/>
                <span className={st.MenuItem__title}>{label}</span>
            </Link>
        </Tooltip>
    )
});

const Menu = ({isExpanded}) => {
    const [selectedItem, setSelectedItem] = useState(null);

    const handleSubMenuItemClick = (label) => {
        setSelectedItem(label);
    };

    return (
        <aside className={cn(st.AsideMenu, {[st.Expanded]: isExpanded})}>
            <div className={st.AsideMenu__container}>
                <Separator bg="--zinc-600"/>
                <header className={st.Header}>
                    <div className={st.UserProfile__panel}>
                        <div>
                            <div className={st.AuthorizedUser}>
                                <div className={st.UserImg}>
                                    <img src="/src/assets/img/img.png" alt=""/>
                                </div>
                                <h3>David</h3>
                                <Button
                                    size={"md"}
                                    isIconOnly={true}
                                    rounded={"sm"}
                                    iconId={"icon_cp_aside-menu_user-setting"}
                                />
                            </div>
                        </div>
                        <Separator bg="--zinc-600"/>
                        <MenuItem label={"Керування акаунтами"} iconId={"icon_cp_aside-menu_users"}
                                  iconY={24} iconX={24}
                                  isExpanded={isExpanded}
                                  link={"/control-panel"}/>
                    </div>
                </header>
                <Separator bg="--zinc-600" mtb={.25}/>
                <main className={st.Body}>
                    <div className={st.Sections}>
                        <MenuAccordionItem label="Сеанси" iconId="icon_cp_aside-menu_sessions" isExpanded={isExpanded}
                                           toolTip={"Сеанси"} position={"right"}>
                            <MenuAccordionSubItem label={"Створити сеанс"}
                                                  iconId="icon_cp_aside-menu_create-sessions"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Створити сеанс"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Створити сеанс"}
                                                  onClick={() => handleSubMenuItemClick("Створити сеанс")}/>
                            <MenuAccordionSubItem label={"Активні сеанси"}
                                                  iconId="icon_cp_aside-menu_active-sessions"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Активні сеанси"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Активні сеанси"}
                                                  onClick={() => handleSubMenuItemClick("Активні сеанси")}/>
                            <MenuAccordionSubItem label={"Неактивні сеанси"}
                                                  iconId="icon_cp_aside-menu_inactive-sessions"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Неактивні сеанси"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Неактивні сеанси"}
                                                  onClick={() => handleSubMenuItemClick("Неактивні сеанси")}/>
                        </MenuAccordionItem>
                        <MenuAccordionItem label="Фільми" iconId="icon_cp_aside-menu_movies" isExpanded={isExpanded}
                                           toolTip={"Фільми"} position={"right"}>
                            <MenuAccordionSubItem label={"Створити фільм"}
                                                  iconId="icon_cp_aside-menu_create-movie"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Створити фільм"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Створити фільм"}
                                                  onClick={() => handleSubMenuItemClick("Створити фільм")}/>
                            <MenuAccordionSubItem label={"Скоро у прокаті"}
                                                  iconId="icon_cp_aside-menu_coming-soon"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Скоро у прокаті"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Скоро у прокаті"}
                                                  onClick={() => handleSubMenuItemClick("Скоро у прокаті")}/>
                            <MenuAccordionSubItem label={"Фільми"}
                                                  iconId="icon_cp_aside-menu_all-movies"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Фільми"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Фільми"}
                                                  onClick={() => handleSubMenuItemClick("Фільми")}/>
                            <MenuAccordionSubItem label={"Архів фільмів"}
                                                  iconId="icon_cp_aside-menu_movies-archive"
                                                  isExpanded={isExpanded}
                                                  toolTip={"Архів фільмів"}
                                                  position={"right"}
                                                  link={"/control-panel"}
                                                  isSelected={selectedItem === "Архів фільмів"}
                                                  onClick={() => handleSubMenuItemClick("Архів фільмів")}/>
                        </MenuAccordionItem>
                    </div>
                </main>
                <Separator bg="--zinc-600"/>
                <footer className={st.Footer}>
                    <div className={st.MenuItems}>
                        <MenuItem label={"На головну"} iconId={"icon_cp_menu-general"} isExpanded={isExpanded}
                                  link={"/"}/>
                        <MenuItem label={"Вихід"} iconId={"icon_cp_menu-logout"} isExpanded={isExpanded}/>
                    </div>
                </footer>
            </div>
        </aside>
    );
};

export default Menu;
