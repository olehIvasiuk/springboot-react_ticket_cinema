import React, {useState} from 'react';
import Button from "@ui/Button/Button.jsx";

const AsideToggle = ({setExpanded}) => {
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setExpanded(prevState => !prevState);
        setOpen(!open)
    };

    return (
        <div>
            <Button
                onClick={handleClick}
                isIconOnly={true}
                rounded={"sm"}
                size={"md"}
                iconId={"icon_cp_aside-toggle"}
            />
        </div>
    );
};

export default AsideToggle;