import React, {useEffect, useState, useCallback, useMemo} from 'react';
import Cropper from 'react-easy-crop';
import Button from "@ui/Button/Button.jsx";
import Slider from "@ui/Slider/Slider.jsx";
import CubeLoading from "@ui/Loader/CubeLoading.jsx";
import {getCroppedImg} from "@utils/FileUtils/imageUtils.js";
import {getFileNameWithoutExtension} from "@utils/FileUtils/fileUtils.js";
import st from "@components/control_panel/image-cropper/ImageCropper.module.css";

const ImageCropper = ({uploadedFile, onClose, handleCropped}) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isImageLoading, setIsImageLoading] = useState(false);
    const [crop, setCrop] = useState({x: 0, y: 0});
    const [zoom, setZoom] = useState(1);
    const [rotation, setRotation] = useState(0);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState({x: 0, y: 0, width: 0, height: 0});
    const [originalImageUrl, setOriginalImageUrl] = useState("");

    const originalFileName = useMemo(() => getFileNameWithoutExtension(uploadedFile), [uploadedFile]);

    const saveUploadedFile = (file) => {
        if (!file) return;
        setIsImageLoading(true);
        try {
            const reader = new FileReader();
            reader.onload = () => {
                setOriginalImageUrl(reader.result.toString());
            };
            reader.onloadend = () => {
                setIsImageLoading(false);
            };
            reader.readAsDataURL(file);
        } catch (error) {
            console.error('Error reading file:', error);
        }
    };

    useEffect(() => {
        if (uploadedFile) {
            saveUploadedFile(uploadedFile);
        }
    }, [uploadedFile]);

    const cropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels);
    }, []);

    const cropImage = useCallback(async () => {
        setIsLoading(true);
        try {
            const {url, file} = await getCroppedImg(
                uploadedFile,
                originalFileName,
                croppedAreaPixels,
                rotation
            );
            handleCropped(url, file);
        } catch (error) {
            console.error('Error cropping image:', error);
        } finally {
            setIsLoading(false);
        }
    }, [uploadedFile, originalFileName, croppedAreaPixels, rotation, handleCropped]);

    const resetSettingsCrop = useCallback(() => {
        setZoom(1);
        setRotation(0);
    }, []);

    const isDefaultCropParameters = useCallback(() => zoom === 1 && rotation === 0, [zoom, rotation]);

    return (
        <div className={st.ImageCropper}>
            <header className={st.Header}>
                <Button onClick={onClose} color="secondary" rounded="full">
                    Назад
                </Button>
                <Button onClick={cropImage} color="primary" rounded="full">
                    Готово
                </Button>
            </header>

            {isLoading && <CubeLoading text="Збереження..."/>}

            <div className={st.CropperArea}>
                {isImageLoading ? (
                    <CubeLoading text="Завантаження.."/>
                ) : (
                    <div className={st.CropperContainer}>
                        <Cropper
                            image={originalImageUrl}
                            crop={crop}
                            zoom={zoom}
                            rotation={rotation}
                            aspect={3 / 4}
                            maxZoom={5}
                            onZoomChange={setZoom}
                            onRotationChange={setRotation}
                            onCropChange={setCrop}
                            onCropComplete={cropComplete}
                            zoomSpeed={0.2}
                            style={{containerStyle: {borderRadius: ".5rem"}}}
                        />
                    </div>
                )}
            </div>

            <footer className={st.Footer}>
                <div className={st.CropDimensions}>
                    <span>Ширина: {croppedAreaPixels.width}px</span>
                    <span>Висота: {croppedAreaPixels.height}px</span>
                </div>
                <div className={st.SliderContainer}>
                    <Slider
                        min={1}
                        max={5}
                        step={0.1}
                        value={zoom}
                        onChange={setZoom}
                        marks={true}
                    />
                    <Slider
                        min={-180}
                        max={180}
                        step={1}
                        value={rotation}
                        onChange={setRotation}
                    />
                </div>
                <div className={st.SubArea}>
                    <Button
                        disabled={isDefaultCropParameters()}
                        onClick={resetSettingsCrop}
                        color="secondary"
                        rounded="full"
                    >
                        Відновити
                    </Button>
                </div>
            </footer>
        </div>
    );
};

export default ImageCropper;