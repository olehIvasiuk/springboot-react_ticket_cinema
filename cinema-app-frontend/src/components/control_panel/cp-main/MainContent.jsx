import React, {memo} from 'react';
import CreateMovie from "../../../pages/control_panel/create-movie/CreateMovie.jsx";

const MainContent = memo(() => {
    return (
        <main className="tt_main-content">
            <CreateMovie/>

        </main>
    );
});

export default MainContent;
