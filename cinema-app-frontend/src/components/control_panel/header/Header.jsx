import React, {memo} from 'react';
import "./Header.css"
import AsideToggle from "../aside-menu/AsideToggle.jsx";


const Header = memo(({setExpanded}) => {

    return (
        <header className="cp_header">
            <div className="cp_header-container">
                <AsideToggle setExpanded={setExpanded}/>
                <div className="cp_header-title">
                    <span className="cp_header-title__text">Панель керування</span>
                </div>
            </div>
        </header>
    );
});

export default Header;