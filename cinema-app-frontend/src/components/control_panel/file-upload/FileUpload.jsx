import React, {useState, memo} from 'react';

import {checkImageDimensions} from "@utils/FileUtils/imageUtils.js";
import {checkFileSize, formatFileSize} from "@utils/FileUtils/fileUtils.js";
import st from "@components/control_panel/file-upload/FileUpload.module.css";
import ImageCropper from "@components/control_panel/image-cropper/ImageCropper.jsx";
import Modal from "@ui/Modal/Modal.jsx";
import ModalContent from "@ui/Modal/ModalContent.jsx";
import ModalHeader from "@ui/Modal/ModalHeader.jsx";
import ModalBody from "@ui/Modal/ModalBody.jsx";
import ModalFooter from "@ui/Modal/ModalFooter.jsx";
import useModal from "@hooks/UseModal/useModal.js";
import FileChooser from "@ui/FileChooser/FileChooser.jsx";
import Button from "@ui/Button/Button.jsx";
import Icon from "@ui/Icon/Icon.jsx";
import cn from "classnames";

const Tip = memo(({isDragActive, isCropped, croppedImage}) => {
    if (isDragActive) {
        return (
            <div className={st.PreviewTip}>
                <span>Відпустіть тут</span>
            </div>
        );
    }

    if (isCropped && croppedImage) {
        return (
            <div className={st.PreviewPost}>
                <img src={croppedImage} alt="poster-preview" decoding="async"/>
            </div>
        );
    }

    return (
        <div className={st.PreviewTip}>
            <Icon iconId={"icon_cp_upload-mng__browse-file"} iconX={80} iconY={80}/>
            <span className={st.TipText}>Перетягніть сюди</span>
            <span className={st.TipText}>або виберіть постер фільму!</span>
            <span className={st.TipText_small}>Максимальний розмір файлу ~15MB</span>
        </div>
    );
});

const FileUpload = memo(() => {
    const [isDragActive, setDragActive] = useState(false);
    const [isCroppingDone, setIsCroppingDone] = useState(false);
    const [croppedImgUrl, setCroppedImgUrl] = useState(null);
    const [croppedFile, setCroppedFile] = useState(null);
    const [croppedFileSize, setCroppedFileSize] = useState('');
    const [croppedFileName, setCroppedFileName] = useState('');
    const [uploadedImageFile, setUploadedImageFile] = useState(null);
    const [error, setError] = useState([]);

    const errorModal = useModal(false);
    const cropperModal = useModal(false);

    const processFile = async (file) => {
        try {
            const [sizeValidation, dimensionValidation] = await Promise.all([
                checkFileSize(file, 15),
                checkImageDimensions(file, 200)
            ]);

            const {isValid: isValidFileSize, errorMessage: sizeErrorMsg} = sizeValidation;
            const {isValid: isValidDimensions, errorMessage: dimensionErrorMsg} = dimensionValidation;

            let newErrors = [];
            if (!isValidFileSize) newErrors = [...newErrors, ...sizeErrorMsg];
            if (!isValidDimensions) newErrors = [...newErrors, ...dimensionErrorMsg];

            if (newErrors.length === 0) {
                setError([]);
                setUploadedImageFile(file)
                cropperModal.openModal()
            } else {
                setError(newErrors);
                errorModal.openModal()
            }
        } catch (error) {
            setError(["Сталася неочікувана помилка.", "Спробуйте ще раз."]);
            errorModal.openModal()
            console.error(error);
        }
    };

    const handleDrag = (e, isEntering) => {
        e.preventDefault();
        setDragActive(isEntering);
    };

    const handleDrop = (e) => {
        e.preventDefault();
        setDragActive(false);
        const file = e.dataTransfer.files[0];
        if (file) {
            processFile(file).catch((err) => {
                console.error('Error processing file:', err);
            });
        }
    };

    const clearSelectedFile = () => {
        setCroppedImgUrl(null);
        setIsCroppingDone(false);
        setCroppedFile(null);
    };

    const handleCropped = (croppedImgUrl, croppedFile) => {
        setCroppedImgUrl(croppedImgUrl);
        setIsCroppingDone(true);
        setCroppedFile(croppedFile);
        setCroppedFileSize(formatFileSize(croppedFile.size));
        setCroppedFileName(croppedFile.name);
        cropperModal.closeModal()
    };

    return (
        <div className={st.UploadPost}>
            <div
                className={cn(st.PreviewArea, {[st.DragEnter]: isDragActive})}
                onDragEnter={(e) => handleDrag(e, true)}
                onDragOver={(e) => handleDrag(e, true)}
                onDragLeave={(e) => handleDrag(e, false)}
                onDrop={handleDrop}
            >
                <Tip isDragActive={isDragActive} isCropped={isCroppingDone} croppedImage={croppedImgUrl}/>
            </div>
            <footer className={st.FooterControls}>
                <Button
                    isIconOnly={true}
                    iconId={"icon_cp_upload-mng__img-crop"}
                    isDisabled={!isCroppingDone}
                    onClick={cropperModal.openModal}
                    color={"primary"}
                    rounded={"sm"}
                />
                <FileChooser
                    color={"primary"}
                    accept={"images"}
                    title={"Прикріпити файл"}
                    onFileUploaded={processFile}
                    iconId={"icon_cp_upload-mng__attach-file"}
                />
                <Button
                    onClick={clearSelectedFile}
                    iconId={"icon_cp_upload-mng__trash-can"}
                    isDisabled={!isCroppingDone}
                    isIconOnly={true}
                    color={"primary"}
                    rounded={"sm"}
                />
                <div className={cn(st.FileInfo, {[st.Expanded]: isCroppingDone})}>
                    <ul className={st.FileInfo_list}>
                        <li className={st.ListItem}>
                            <span className={st.Key}>Ім'я файлу: </span>
                            <span className={st.Value} title={croppedFileName}>{croppedFileName}</span>
                        </li>
                        <li className={st.ListItem}>
                            <span className={st.Key}>Розмір: </span>
                            <span className={st.Value} title={croppedFileSize}>{croppedFileSize}</span>
                        </li>
                    </ul>
                </div>
            </footer>
            <Modal
                isOpen={cropperModal.isOpen}
                onClose={cropperModal.closeModal}
                hideCloseButton={true}
                ariaLabelledBy={"Image Crop Modal"}
                size="auto"
                backdrop={"darken"}
                rounded={"xl"}
                animationVariant={"slide"}
            >
                {({onClose}) => (
                    <ModalContent>
                        <ImageCropper
                            uploadedFile={uploadedImageFile}
                            onClose={onClose}
                            handleCropped={handleCropped}
                        />
                    </ModalContent>
                )}
            </Modal>

            <Modal
                isOpen={errorModal.isOpen}
                onClose={errorModal.closeModal}
                backdrop={"darken"}
                isDismissible={false}
                isEscDismissible={false}
                hideCloseButton={true}
            >
                {({onClose}) =>
                    <ModalContent>
                        <ModalHeader>
                            <Icon
                                color={"#ffcf00"}
                                iconY={32}
                                iconX={32}
                                iconId={"icon_alert-triangle"}
                            />
                            <h3>Помилка</h3>
                        </ModalHeader>
                        <ModalBody>
                            {error.map((item, i) => (
                                <p key={i}>{item}</p>
                            ))}
                        </ModalBody>
                        <ModalFooter className={st.ErrorFooter}>
                            <Button onClick={onClose} color={"primary"}>Ок</Button>
                        </ModalFooter>
                    </ModalContent>
                }
            </Modal>
        </div>
    );
});

export default FileUpload;