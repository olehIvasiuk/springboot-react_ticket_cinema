import axios from 'axios';
import { API_URL, ROUTES } from '../constants/constants';

const axiosInstance = axios.create({
    baseURL: API_URL,
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json',
    },
});

class SearchService {
    async searchMovies(query){
        try {
            const response = await axiosInstance.post(ROUTES.HOME, {}, {
                params: { query },
            });
            return response.data;
        } catch (error) {
            throw new Error('Ошибка');
        }
    }
}

export default new SearchService();