import {useLayoutEffect} from "react";
import {useStyle} from "@hooks/UseStyle/useStyle.js";

export const useDisableScroll = (isOpen) => {
    const [, setOverflow] = useStyle("overflow");
    const [, setBodyWidth] = useStyle("width", document.body);

    useLayoutEffect(() => {
        if (isOpen) {
            const scrollBarWidth = window.innerWidth - document.documentElement.clientWidth;
            setBodyWidth(`calc(100% - ${scrollBarWidth}px)`);
            setOverflow("hidden");
        } else {
            setBodyWidth("100%");
            setOverflow("auto");
        }

        return () => {
            setBodyWidth("100%");
            setOverflow("auto");
        };
    }, [isOpen, setOverflow, setBodyWidth]);
};