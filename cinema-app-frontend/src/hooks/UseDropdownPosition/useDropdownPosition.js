import {useState, useEffect, useRef} from "react";

export const useDropdownPosition = (selectRef, dropdownRef, isOpen) => {
    const [dropdownPosition, setDropdownPosition] = useState({top: 0, left: 0, width: 0});
    const [isDropdownBelow, setIsDropdownBelow] = useState(true);
    const observerRef = useRef(null);

    useEffect(() => {
        const updatePosition = () => {
            if (selectRef.current && dropdownRef.current) {
                const selectRect = selectRef.current.getBoundingClientRect();
                const dropdownHeight = dropdownRef.current.offsetHeight;
                const viewportHeight = window.innerHeight;

                const spaceBelow = viewportHeight - selectRect.bottom;
                const spaceAbove = selectRect.top;

                const shouldPlaceBelow = spaceBelow >= dropdownHeight || spaceBelow >= spaceAbove;

                setIsDropdownBelow(shouldPlaceBelow);

                const top = (shouldPlaceBelow ? selectRect.bottom + 6 : selectRect.top - dropdownHeight - 6) + window.scrollY;
                const left = selectRect.left;
                const width = selectRect.width;

                setDropdownPosition({top, left, width});
            }
        };

        if (isOpen) {
            updatePosition();

            observerRef.current = new MutationObserver(() => {
                if (dropdownRef.current) {
                    updatePosition();
                }
            });

            observerRef.current.observe(document.body, {childList: true, subtree: true});
            window.addEventListener("resize", updatePosition);
            window.addEventListener("scroll", updatePosition);

            return () => {
                if (observerRef.current) {
                    observerRef.current.disconnect();
                }
                window.removeEventListener("resize", updatePosition);
                window.removeEventListener("scroll", updatePosition);
            };
        }
    }, [isOpen, selectRef, dropdownRef]);

    return {dropdownPosition, isDropdownBelow};
};