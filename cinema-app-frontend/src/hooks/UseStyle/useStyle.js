import {useState, useEffect} from "react";

export const useStyle = (prop, $ = document.body) => {
    const [value, setValue] = useState(getComputedStyle($).getPropertyValue(prop));

    useEffect(() => {
        $.style.setProperty(prop, value);
    }, [value, prop, $]);

    return [value, setValue];
};