import React from 'react'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import ReactDOM from 'react-dom/client'
import Main from "./components/site/main/Main.jsx";
import "./assets/styles/reset-styles.css"
import "./assets/styles/cite-global.css"
import General from "./pages/control_panel/general/General.jsx";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Main/>
    },
    {
        path: "/control-panel",
        element: <General/>
    }
])

ReactDOM
    .createRoot(document.getElementById('root'))
    .render(
        <React.StrictMode>
            <RouterProvider router={router}/>
        </React.StrictMode>,
    )
