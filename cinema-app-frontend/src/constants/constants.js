const API_URL = 'http://localhost:8080/api';

const ROUTES = {
    HOME: '/',
    MOVIES: '/movies',
};

export {
    API_URL,
    ROUTES,
}
