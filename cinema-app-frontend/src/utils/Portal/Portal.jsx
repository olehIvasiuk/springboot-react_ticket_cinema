import { useState, useEffect } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";

const Portal = ({ children, containerElement }) => {
    const [container, setContainer] = useState(null);

    useEffect(() => {
        setContainer(containerElement);
    }, [containerElement]);

    if (!container) {
        return null;
    }

    if (!(container instanceof Element || container instanceof DocumentFragment)) {
        console.error("Invalid container type: ", container);
        return null;
    }

    return createPortal(
        <div tabIndex={-1}>
            {children}
        </div>,
        container
    );
};

Portal.propTypes = {
    children: PropTypes.node.isRequired,
    containerElement: PropTypes.instanceOf(Element).isRequired,
};

export default Portal;
