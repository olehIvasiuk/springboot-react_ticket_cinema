/**
 * Checks the file size.
 * @param {File} file - The file to check.
 * @param {number} size - The maximum allowed file size in megabytes (MB).
 * @returns {Object} The result of the check:
 *   - isValid {boolean} - Whether the file size is within the allowed limit.
 *   - errorMessage {string[]} - An array of error messages (if the file size exceeds the allowed limit).
 */
export const checkFileSize = (file, size) => {
    const MAX_SIZE = size * 1024 * 1024;
    if (file.size > MAX_SIZE) {
        return {isValid: false, errorMessage: [`Розмір файлу перевищує ${size}MB`]};
    }
    return {isValid: true, errorMessage: [""]};
};

/**
 * Reads the file and returns its content as a Data URL.
 * @param {File} file - The file to read.
 * @returns {Promise<string>} A promise that resolves with the file's content as a Data URL.
 * @throws {Error} If the file reading fails.
 */
export const readFileAsDataURL = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            resolve(reader.result);
        };
        reader.onerror = () => {
            reject(new Error("Failed to read file"));
        };
        reader.readAsDataURL(file);
    });
};

/**
 * Returns the file name without extension
 * @param {File} file - Input file
 * @returns {string|null} File name without extension
 */
export const getFileNameWithoutExtension = (file) => {
    if (!file) return null;
    return file.name.split('.').slice(0, -1).join('.');
};

/**
 * Formats the file size
 * @param {number} bytes - File size in bytes
 * @returns {string} - Formatted file size
 */
export const formatFileSize = (bytes) => {
    const sizes = ['Bytes', 'KB', 'MB'];
    if (bytes === 0) return '0 Byte';
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    return `${(bytes / Math.pow(1024, i)).toFixed(2)} ${sizes[i]}`;
};