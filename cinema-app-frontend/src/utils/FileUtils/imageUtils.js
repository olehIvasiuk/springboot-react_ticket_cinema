/**
 * Checks if the browser supports WebP format
 * @returns {Promise<boolean>} Promise that resolves to true if WebP is supported, otherwise false
 */
let isWebPSupported = null;
const checkWebPSupport = () => {
    if (isWebPSupported === null) {
        const canvas = document.createElement('canvas');
        canvas.width = 1;
        canvas.height = 1;
        isWebPSupported = new Promise((resolve) => {
            canvas.toBlob((blob) => {
                resolve(blob && blob.type === "image/webp");
            }, "image/webp");
        });
    }
    return isWebPSupported;
};

/**
 * Creates an Image object from a File
 * @param {File} file - Input file
 * @returns {Promise<HTMLImageElement>} Promise that resolves to the created Image object
 */
export const createImage = (file) => {
    return new Promise((resolve, reject) => {
        const image = new Image();
        image.setAttribute("crossOrigin", "anonymous");
        image.onload = () => {
            URL.revokeObjectURL(image.src);
            resolve(image);
        };
        image.onerror = () => {
            URL.revokeObjectURL(image.src);
            reject(new Error('Failed to load image'));
        };
        image.src = URL.createObjectURL(file);
    });
};

/**
 * Checks if the image dimensions meet the minimum required dimensions
 * @param {File} file - Input file
 * @param {number} minDimension - Minimum dimension (width and height) in pixels
 * @returns {Promise<{isValid: boolean, errorMessage: string[]}>} Promise that resolves to an object indicating if the image is valid and any error messages
 */
export const checkImageDimensions = async (file, minDimension) => {
    try {
        const image = await createImage(file);
        const {naturalWidth, naturalHeight} = image;
        if (naturalWidth < minDimension || naturalHeight < minDimension) {
            return {
                isValid: false,
                errorMessage: [
                    "Розміри зображення замалі.",
                    `Мінімальні розміри: ${minDimension}x${minDimension} пікселів.`
                ]
            };
        }
        return {
            isValid: true,
            errorMessage: [""]
        };
    } catch (error) {
        console.error('Error checking file dimensions:', error);
        return {
            isValid: false,
            errorMessage: ["Не вдалося перевірити розміри зображення."]
        };
    }
};

/**
 * Converts an angle from degrees to radians
 * @param {number} degreeValue - Angle in degrees
 * @returns {number} Angle in radians
 */
export const getRadianAngle = (degreeValue) => {
    return (degreeValue * Math.PI) / 180;
};

/**
 * Calculates the new dimensions of an image after rotation
 * @param {number} width - Original width of the image
 * @param {number} height - Original height of the image
 * @param {number} rotation - Rotation angle in degrees
 * @returns {{width: number, height: number}} New dimensions of the rotated image
 */
const rotateSize = (width, height, rotation) => {
    const rotRad = getRadianAngle(rotation);
    return {
        width: Math.abs(Math.cos(rotRad) * width) + Math.abs(Math.sin(rotRad) * height),
        height: Math.abs(Math.sin(rotRad) * width) + Math.abs(Math.cos(rotRad) * height),
    };
};

/**
 * Crops and rotates an image, and optionally flips it horizontally or vertically
 * @param {File} file - Input file
 * @param {string} fileName - Name of the resulting file
 * @param {{x: number, y: number, width: number, height: number}} pixelCrop - Crop dimensions
 * @param {number} [rotation=0] - Rotation angle in degrees
 * @param {{horizontal: boolean, vertical: boolean}} [flip={horizontal: false, vertical: false}] - Flip options
 * @returns {Promise<{file: File | null, url: string | null}>} Promise that resolves to an object with the cropped file and its URL
 */
export const getCroppedImg = async (
    file,
    fileName,
    pixelCrop,
    rotation = 0,
    flip = {horizontal: false, vertical: false}
) => {
    try {
        const image = await createImage(file);
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");

        if (!ctx) {
            throw new Error('Failed to get canvas context');
        }

        const rotRad = getRadianAngle(rotation);
        const {width: bBoxWidth, height: bBoxHeight} = rotateSize(image.width, image.height, rotation);

        canvas.width = bBoxWidth;
        canvas.height = bBoxHeight;

        ctx.translate(bBoxWidth / 2, bBoxHeight / 2);
        ctx.rotate(rotRad);
        ctx.scale(flip.horizontal ? -1 : 1, flip.vertical ? -1 : 1);
        ctx.translate(-image.width / 2, -image.height / 2);
        ctx.imageSmoothingQuality = "high";

        ctx.drawImage(image, 0, 0);

        const data = ctx.getImageData(pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height);
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        ctx.putImageData(data, 0, 0);

        const webPSupported = await checkWebPSupport();
        return new Promise((resolve, reject) => {
            const mimeType = webPSupported ? "image/webp" : "image/jpeg";
            const extension = webPSupported ? "webp" : "jpeg";

            canvas.toBlob((blob) => {
                if (blob) {
                    const newFile = new File([blob], `${fileName}.${extension}`, {type: mimeType});
                    resolve({file: newFile, url: URL.createObjectURL(newFile)});
                } else {
                    reject(new Error("Blob creation failed."));
                }
            }, mimeType, .92);
        });
    } catch (error) {
        console.error('Error creating cropped image:', error);
        return {file: null, url: null};
    }
};